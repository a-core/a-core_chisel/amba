addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.1.2")
addSbtPlugin("com.github.sbt" % "sbt-git" % "2.0.1")
addSbtPlugin("org.scala-sbt" % "sbt-maven-resolver" % "0.1.0")
ThisBuild / libraryDependencySchemes += "org.scala-lang.modules" %% "scala-xml" % VersionScheme.Always