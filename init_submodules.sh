#!/usr/bin/env bash
#Init submodules in this dir, if any
DIR="$( cd "$( dirname $0 )" && pwd )"
git submodule sync

#Recursively init submodules
SUBMODULES="\
    "
for module in $SUBMODULES; do
    git submodule update --init ${module}
    cd ${DIR}/${module}
    if [ -f "./init_submodules.sh" ]; then
        ./init_submodules.sh
    fi

    if [ -f "build.sbt" ]; then
        sbt publishLocal
    fi
    cd ${DIR}
done


exit 0
