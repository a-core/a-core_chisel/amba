# Set paths for Mentor programs (Eldo)
use advms_17.1

set toolpaths="\
    /prog/riscv/yosys/bin \
    /prog/riscv/SymbiYosys/bin \
    /prog/riscv/boolector/bin \
"

foreach toolpath ($toolpaths)
    if ($toolpath != "") then
        if ( "${PATH}" !~ *"$toolpath"*) then
            echo "Adding $toolpath to PATH"
            setenv PATH ${toolpath}:${PATH}
        else
            echo "$toolpath already in path"
        endif
    endif
end

unset toolpaths

