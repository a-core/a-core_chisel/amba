# Library for AMBA memory mapped peripherals and interconnects

## [API Documentation](https://a-core.gitlab.io/a-core_chisel/amba/)

## Publishing

Amba is automatically published as a Maven package. The version number is of the following format: `x.y-n-sha`, where `x` is major version, `y` is minor version, `x.y` is the same as an existing git tag, `n` is a count of how many commits the version has moved forward since the tag, and `sha` points to the commit hash of the git commit that the release was made of. 

For more information and publishing practices, see [A-Core Chisel developer's guide](https://gitlab.com/a-core/a-core_documentation/tutorials/-/blob/master/a-core_chisel_guide.md#5-scala-libraries).


