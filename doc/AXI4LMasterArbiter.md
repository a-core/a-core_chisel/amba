# AXI4-Lite Master Arbiter

The structure is mostly similar to https://github.com/pulp-platform/axi/blob/master/src/axi_lite_mux.sv

## Write channel
![Write channel](../diagrams/axi4lmasterarbiter_w.svg)

## Read channel
![Read channel](../diagrams/axi4lmasterarbiter_r.svg)