# AXI4L Demultiplexer

The structure is mostly similar to https://github.com/pulp-platform/axi/blob/master/src/axi_lite_demux.sv

## Write channel
![Write channel](../diagrams/axi4ldemux_w.svg)

## Read channel
![Read channel](../diagrams/axi4ldemux_r.svg)