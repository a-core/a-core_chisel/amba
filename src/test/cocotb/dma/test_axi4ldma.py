# SPDX-License-Identifier: Apache-2.0

# for list type hints
from __future__ import annotations

# pip install cocotb
import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, with_timeout, RisingEdge

# pip install cocotbext-axi
from cocotbext.axi import AxiLiteBus, AxiLiteRam, AxiLiteMaster

memory_map = {
    "slave_0": 0x0000_1000,  # python model
    "slave_1": 0x0000_2000,  # python model
    "slave_2": 0x0000_3000,  # RTL model AXI4LSRAM
    "slave_3": 0x0000_4000,  # RTL model AXI4LSRAM
}

# DMA control registers
control_mmap = {
    "read_start_addr": 0x0000,
    "read_end_addr": 0x0004,
    "write_start_addr": 0x0008,
    "write_end_addr": 0x000C,
    "read_increment": 0x0010,
    "write_increment": 0x0014,
    "control": 0x0018,
    "status": 0x001C,
}

simple_data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

# 32 pieces of random 8-bit data
random_data = [random.randint(0, 255) for _ in range(32)]


# Define test case parameters
class dma_test_case:
    def __init__(
        self,
        read_target: str,
        write_target: str,
        read_start_addr: int,
        write_start_addr: int,
        read_end_addr: int,
        write_end_addr: int,
        read_incr: int,
        write_incr: int,
        end_condition: int,
        read_data_bytes: list[int],
    ):
        self.read_target = read_target
        self.write_target = write_target
        self.read_start_addr = read_start_addr
        self.write_start_addr = write_start_addr
        self.read_end_addr = read_end_addr
        self.write_end_addr = write_end_addr
        self.read_incr = read_incr
        self.write_incr = write_incr
        self.end_condition = end_condition
        self.read_data_bytes = read_data_bytes


# Define various test cases
test_cases: list[dma_test_case] = [
    dma_test_case(
        read_target="slave_0",
        write_target="slave_1",
        read_start_addr=0,
        read_end_addr=12,
        write_start_addr=0,
        write_end_addr=24,
        read_incr=4,
        write_incr=8,
        end_condition=3,
        read_data_bytes=simple_data,
    ),
    dma_test_case(
        read_target="slave_0",
        write_target="slave_2",
        read_start_addr=0,
        read_end_addr=12,
        write_start_addr=0,
        write_end_addr=12,
        read_incr=4,
        write_incr=4,
        end_condition=3,
        read_data_bytes=simple_data,
    ),
    dma_test_case(
        read_target="slave_2",
        write_target="slave_0",
        read_start_addr=0,
        read_end_addr=12,
        write_start_addr=12,
        write_end_addr=36,
        read_incr=4,
        write_incr=8,
        end_condition=3,
        read_data_bytes=simple_data,
    ),
    dma_test_case(
        read_target="slave_2",
        write_target="slave_3",
        read_start_addr=0,
        read_end_addr=16,
        write_start_addr=0,
        write_end_addr=8,
        read_incr=8,
        write_incr=4,
        end_condition=3,
        read_data_bytes=simple_data,
    ),
    dma_test_case(
        read_target="slave_2",
        write_target="slave_0",
        read_start_addr=0,
        read_end_addr=8,
        write_start_addr=12,
        write_end_addr=36,
        read_incr=4,
        write_incr=8,
        end_condition=2,
        read_data_bytes=simple_data,
    ),
    dma_test_case(
        read_target="slave_3",
        write_target="slave_2",
        read_start_addr=0,
        read_end_addr=16,
        write_start_addr=0,
        write_end_addr=8,
        read_incr=8,
        write_incr=4,
        end_condition=3,
        read_data_bytes=simple_data,
    ),
]


@cocotb.test()
async def test_axi4ldma(dut):
    """
    Generic test to test reading and writing to/from arbitrary slave modules.
    See dma_test_case and test_cases above.

    You can test with:
        * Same sized read and write spaces:
          `(read_end-read_start)/read_incr == (write_end-write_start)/write_incr`.
          Must use end_condition=(1,2,3)
        * Larger write space than read space
          `(write_end-write_start)/write_incr > (read_end-read_start)/read_incr`.
          Must use end_condition=2
    """
    clock = dut.clock
    reset = dut.reset
    dut.io_start_trigger.value = 0
    reset.value = 0
    axi_slaves = []
    for i in range(2):
        axi_slaves.append(
            AxiLiteRam(
                AxiLiteBus.from_prefix(dut, f"io_axi_slave_{i}"),
                clock,
                reset,
                reset_active_level=True,
                size=2**6,
            )
        )

    axilite_master = AxiLiteMaster(
        AxiLiteBus.from_prefix(dut, "io_control_port"),
        clock,
        reset,
        reset_active_level=True,
    )

    # Start clock
    cocotb.start_soon(Clock(clock, 2, units="step").start())

    for nt, test_case in enumerate(test_cases):
        dut._log.info("Test case " + str(nt))

        # Initialize memory with given data
        if test_case.read_target == "slave_0":
            data_idx = 0
            for i in range(
                test_case.read_start_addr,
                test_case.read_end_addr + 1,
                test_case.read_incr,
            ):
                axi_slaves[0].write_words(
                    i, test_case.read_data_bytes[data_idx : data_idx + 4], ws=1
                )
                data_idx += 4
        if test_case.read_target == "slave_1":
            data_idx = 0
            for i in range(
                test_case.read_start_addr,
                test_case.read_end_addr + 1,
                test_case.read_incr,
            ):
                axi_slaves[1].write_words(
                    i, test_case.read_data_bytes[data_idx : data_idx + 4], ws=1
                )
                data_idx += 4
        if test_case.read_target == "slave_2":
            data_idx = 0
            mem_blocks = [
                dut.mem1.mem_array_0,
                dut.mem1.mem_array_1,
                dut.mem1.mem_array_2,
                dut.mem1.mem_array_3,
            ]
            words = []
            for i in range(
                test_case.read_start_addr,
                test_case.read_end_addr + 4,
                test_case.read_incr,
            ):
                for j in range(4):
                    mem_blocks[j][
                        i // len(mem_blocks)
                    ].value = test_case.read_data_bytes[data_idx]
                    data_idx += 1
        if test_case.read_target == "slave_3":
            data_idx = 0
            mem_blocks = [
                dut.mem2.mem_array_0,
                dut.mem2.mem_array_1,
                dut.mem2.mem_array_2,
                dut.mem2.mem_array_3,
            ]
            words = []
            for i in range(
                test_case.read_start_addr,
                test_case.read_end_addr + 4,
                test_case.read_incr,
            ):
                for j in range(4):
                    mem_blocks[j][
                        i // len(mem_blocks)
                    ].value = test_case.read_data_bytes[data_idx]
                    data_idx += 1

        # Reset
        await ClockCycles(clock, 2)
        reset.value = 1
        await ClockCycles(clock, 2)
        reset.value = 0
        await ClockCycles(clock, 2)

        # Write control values via AXI
        await with_timeout(
            axilite_master.init_write(
                control_mmap["read_start_addr"],
                (
                    memory_map[test_case.read_target] + test_case.read_start_addr
                ).to_bytes(4, "little"),
            ).wait(),
            50,
            timeout_unit="step",
        )
        await with_timeout(
            axilite_master.init_write(
                control_mmap["read_end_addr"],
                (memory_map[test_case.read_target] + test_case.read_end_addr).to_bytes(
                    4, "little"
                ),
            ).wait(),
            50,
            timeout_unit="step",
        )
        await with_timeout(
            axilite_master.init_write(
                control_mmap["read_increment"],
                test_case.read_incr.to_bytes(4, "little"),
            ).wait(),
            50,
            timeout_unit="step",
        )
        await with_timeout(
            axilite_master.init_write(
                control_mmap["write_start_addr"],
                (
                    memory_map[test_case.write_target] + test_case.write_start_addr
                ).to_bytes(4, "little"),
            ).wait(),
            50,
            timeout_unit="step",
        )
        await with_timeout(
            axilite_master.init_write(
                control_mmap["write_end_addr"],
                (
                    memory_map[test_case.write_target] + test_case.write_end_addr
                ).to_bytes(4, "little"),
            ).wait(),
            50,
            timeout_unit="step",
        )
        await with_timeout(
            axilite_master.init_write(
                control_mmap["write_increment"],
                test_case.write_incr.to_bytes(4, "little"),
            ).wait(),
            50,
            timeout_unit="step",
        )

        # Start DMA transfer by writing to control register
        dut._log.info("Used control register for starting DMA transaction")
        await with_timeout(
            axilite_master.init_write(
                control_mmap["control"],
                bytearray.fromhex(f"00010{str(test_case.end_condition)}00"),
            ).wait(),
            50,
            timeout_unit="step",
        )

        await ClockCycles(clock, 1)

        dut.io_start_trigger.value = 0

        await with_timeout(
            RisingEdge(dut.io_done_trigger), 10000, timeout_unit="step"
        )

        await ClockCycles(clock, 10)

        # Check if the memory got transferred correctly
        words = []
        if test_case.write_target == "slave_0":
            for i in range(
                test_case.write_start_addr,
                test_case.write_end_addr + 4,
                test_case.write_incr,
            ):
                word = axi_slaves[0].read_words(i, 4, ws=1)
                for j in range(4):
                    words.append(word[j])
        if test_case.write_target == "slave_1":
            for i in range(
                test_case.write_start_addr,
                test_case.write_end_addr + 4,
                test_case.write_incr,
            ):
                word = axi_slaves[1].read_words(i, 4, ws=1)
                for j in range(4):
                    words.append(word[j])
        if test_case.write_target == "slave_2":
            mem_blocks = [
                dut.mem1.mem_array_0,
                dut.mem1.mem_array_1,
                dut.mem1.mem_array_2,
                dut.mem1.mem_array_3,
            ]
            words = []
            for i in range(
                test_case.write_start_addr,
                test_case.write_end_addr + 4,
                test_case.write_incr,
            ):
                for j in range(4):
                    words.append(mem_blocks[j][i // len(mem_blocks)].value.integer)
        if test_case.write_target == "slave_3":
            mem_blocks = [
                dut.mem2.mem_array_0,
                dut.mem2.mem_array_1,
                dut.mem2.mem_array_2,
                dut.mem2.mem_array_3,
            ]
            words = []
            for i in range(
                test_case.write_start_addr,
                test_case.write_end_addr + 4,
                test_case.write_incr,
            ):
                for j in range(4):
                    words.append(mem_blocks[j][i // len(mem_blocks)].value.integer)

        dut._log.info("Read data: " + str(test_case.read_data_bytes))
        dut._log.info("Write data: " + str(words))
        read_final_idx = ((test_case.read_end_addr - test_case.read_start_addr) / (
            test_case.read_incr
        )) * 4 + 3
        read_idx = 0
        for i in range(len(words)):
            assert words[i] == test_case.read_data_bytes[read_idx], (
                f"Byte number {i} not the same!"
                + f"Read: {test_case.read_data_bytes[read_idx]}, Wrote: {words[i]}"
            )
            if read_idx == read_final_idx:
                read_idx = 0
            else:
                read_idx += 1


@cocotb.test()
async def test_interleaving(dut):
    """
    Writes from slave0 to slave1, slave2, and slave3 in a time-interleaved fashion.
    It defines a read space that is twice larger than the write space.
    This ends up so that the write address loops twice over its address space before
    read address reaches its end.
    It ends up writing a different value twice to the same address in each slave.
    Therefore, only the latter is expected to get stored.
    End_condition is set to READ mode, meaning that the DMA will transfer until read
    address reaches its end address.
    """
    clock = dut.clock
    reset = dut.reset
    reset.value = 0
    axi_slaves = []
    for i in range(2):
        axi_slaves.append(
            AxiLiteRam(
                AxiLiteBus.from_prefix(dut, f"io_axi_slave_{i}"),
                clock,
                reset,
                reset_active_level=True,
                size=2**6,
            )
        )

    axilite_master = AxiLiteMaster(
        AxiLiteBus.from_prefix(dut, "io_control_port"),
        clock,
        reset,
        reset_active_level=True,
    )

    # Start clock
    cocotb.start_soon(Clock(clock, 2, units="step").start())

    dut.io_start_trigger.value = 0

    # Initialize data to slave0
    data_idx = 0
    for i in range(0, 33, 4):
        axi_slaves[0].write_words(i, random_data[data_idx : data_idx + 4], ws=1)
        data_idx += 4

    # Reset
    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    reset.value = 0
    await ClockCycles(clock, 2)

    # Write control values via AXI
    await with_timeout(
        axilite_master.init_write(
            control_mmap["read_start_addr"],
            (memory_map["slave_0"]).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )
    await with_timeout(
        axilite_master.init_write(
            control_mmap["read_end_addr"],
            (memory_map["slave_0"] + 20).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )
    await with_timeout(
        axilite_master.init_write(
            control_mmap["read_increment"],
            (4).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )
    await with_timeout(
        axilite_master.init_write(
            control_mmap["write_start_addr"],
            (memory_map["slave_1"]).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )
    await with_timeout(
        axilite_master.init_write(
            control_mmap["write_end_addr"],
            (memory_map["slave_3"]).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )
    await with_timeout(
        axilite_master.init_write(
            control_mmap["write_increment"],
            (memory_map["slave_3"] - memory_map["slave_2"]).to_bytes(4, "little"),
        ).wait(),
        50,
        timeout_unit="step",
    )

    # Start transaction
    # Set end condition to EITHER
    dut._log.info("Used control register for starting DMA transaction")
    await with_timeout(
        axilite_master.init_write(
            control_mmap["control"],
            bytearray.fromhex("00010100"),
        ).wait(),
        50,
        timeout_unit="step",
    )

    # Wait until done-signal goes high
    await with_timeout(RisingEdge(dut.io_done_trigger), 10000, timeout_unit="step")

    # Some clock cycles needed after done-signal
    # so that data arrives in the memory
    await ClockCycles(clock, 20)

    word = axi_slaves[1].read_words(0, 4, ws=1)
    for i in range(0, 4):
        assert word[i] == random_data[12+i]

    mem_blocks = [
        dut.mem1.mem_array_0,
        dut.mem1.mem_array_1,
        dut.mem1.mem_array_2,
        dut.mem1.mem_array_3,
    ]
    for i in range(4):
        assert mem_blocks[i][0].value.integer == random_data[12 + 4 + i]

    mem_blocks = [
        dut.mem2.mem_array_0,
        dut.mem2.mem_array_1,
        dut.mem2.mem_array_2,
        dut.mem2.mem_array_3,
    ]
    for i in range(4):
        assert mem_blocks[i][0].value.integer == random_data[12 + 8 + i]
