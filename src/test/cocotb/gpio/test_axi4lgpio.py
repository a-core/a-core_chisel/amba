# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, with_timeout
# pip install cocotbext-axi
from cocotbext.axi import AxiLiteMaster, AxiLiteBus
from numpy import byte

memory_map = {"in_vec"  : 0x0000_0000,
              "out_vec" : 0x0000_0010}

@cocotb.test()
async def test_axi4lgpio(dut):
    print("Hello world")
    aclk = dut.axi_io_aclk
    arstn = dut.axi_io_aresetn
    arstn.value = 1
    axilite_master = AxiLiteMaster(AxiLiteBus.from_prefix(dut, "axi_io"), aclk, arstn, reset_active_level=False)
    cocotb.start_soon(Clock(aclk, 1, units="us").start())
    await ClockCycles(aclk, 2)
    arstn.value = 0
    await ClockCycles(aclk, 2)
    arstn.value = 1
    await ClockCycles(aclk, 2)
    dut.io_io_in_vec.value = 0
    assert(dut.io_io_out_vec == 0)

    dut._log.info("Test write to output register") 
    test_write_data = bytearray.fromhex("ABCDEF00")
    await with_timeout(axilite_master.init_write(memory_map["out_vec"], test_write_data).wait(), 50, timeout_unit='us')
    data = axilite_master.init_read(memory_map["out_vec"], length=4)
    await with_timeout(data.wait(), 50, timeout_unit='us')
    assert(data.data.data == test_write_data)
    assert(dut.io_io_out_vec == int.from_bytes(test_write_data, byteorder='little'))

    dut._log.info("Test unaligned write to output register") 
    test_write_data = bytearray.fromhex("00000000")
    await with_timeout(axilite_master.init_write(memory_map["out_vec"]+2, test_write_data).wait(), 50, timeout_unit='us')
    data = axilite_master.init_read(memory_map["out_vec"], length=4)
    await with_timeout(data.wait(), 50, timeout_unit='us')
    assert(data.data.data == bytearray.fromhex("ABCD0000"))
    assert(dut.io_io_out_vec == int.from_bytes(bytearray.fromhex("ABCD0000"), byteorder='little'))

    dut._log.info("Test reading input register for some values")
    test_read_data = bytearray.fromhex("FAFBFCFD")
    dut.io_io_in_vec.value = int.from_bytes(test_read_data, byteorder="little")
    # Two clock cycle delay needed because of a synchronizing register
    await ClockCycles(aclk, 2)
    data = axilite_master.init_read(memory_map["in_vec"], length=4)
    await with_timeout(data.wait(), 50, timeout_unit='us')
    assert(data.data.data == test_read_data)
