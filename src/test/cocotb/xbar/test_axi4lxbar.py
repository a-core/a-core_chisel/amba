# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, with_timeout, Join

# pip install cocotbext-axi
from cocotbext.axi import AxiLiteMaster, AxiLiteBus, AxiLiteRam

memory_map = {"slave_0": 0x0000_1000, "slave_1": 0x0000_2000, "slave_2": 0x0000_3000}


@cocotb.test()
async def test_axi4lgpio(dut):
    clock = dut.axi_master_io_0_aclk
    arstn = dut.axi_master_io_0_aresetn
    arstn.value = 1
    n_masters = 2
    axi_masters = []
    axi_slaves = []
    axi_masters.append(
        AxiLiteMaster(
            AxiLiteBus.from_prefix(dut, "axi_master_io_0"),
            clock,
            arstn,
            reset_active_level=False,
        )
    )
    axi_masters.append(
        AxiLiteMaster(
            AxiLiteBus.from_prefix(dut, "axi_master_io_1"),
            clock,
            arstn,
            reset_active_level=False,
        )
    )
    axi_slaves.append(
        AxiLiteRam(
            AxiLiteBus.from_prefix(dut, "axi_slave_io_0"),
            clock,
            arstn,
            reset_active_level=False,
            size=2**8,
        )
    )
    axi_slaves.append(
        AxiLiteRam(
            AxiLiteBus.from_prefix(dut, "axi_slave_io_1"),
            clock,
            arstn,
            reset_active_level=False,
            size=2**8,
        )
    )
    axi_slaves.append(
        AxiLiteRam(
            AxiLiteBus.from_prefix(dut, "axi_slave_io_2"),
            clock,
            arstn,
            reset_active_level=False,
            size=2**8,
        )
    )
    cocotb.start_soon(Clock(clock, 2, units="step").start())
    await ClockCycles(clock, 2)
    arstn.value = 0
    await ClockCycles(clock, 2)
    arstn.value = 1
    await ClockCycles(clock, 2)

    dut._log.info("Test single writes from all masters to all slaves")
    for i in range(0, n_masters):
        for j in range(0, 3):
            dut._log.info(f"Test write: m{i} -> s{j}")
            test_write_data = bytearray.fromhex(f"ABCDEF{i}{j}")
            await cocotb.start_soon(
                axi_lite_transaction(
                    axi_masters[i],
                    memory_map[f"slave_{j}"],
                    wdata=test_write_data,
                    write=True,
                )
            )
            assert axi_slaves[j].read(0x00, 4) == test_write_data

    await ClockCycles(clock, 2)

    dut._log.info("Test writing to the same slave, different address, at the same time")
    tasks = []
    for i in range(0, n_masters):
        dut._log.info(f"Test write: m{i} -> s0")
        test_write_data = bytearray.fromhex(f"0000000{i+1}")
        tasks.append(
            cocotb.start_soon(
                axi_lite_transaction(
                    axi_masters[i],
                    memory_map["slave_0"] + (i + 1) * 4,
                    wdata=test_write_data,
                    write=True,
                )
            )
        )
    for task in tasks:
        await Join(task)
    for i in range(0, n_masters):
        test_write_data = bytearray.fromhex(f"0000000{i+1}")
        assert axi_slaves[0].read((i + 1) * 4, 4) == test_write_data
    await ClockCycles(clock, 2)

    dut._log.info("Test writing to different slaves at the same time")
    tasks = []
    for i in range(0, n_masters):
        dut._log.info(f"Test write: m{i} -> s{i}")
        test_write_data = bytearray.fromhex(f"0000000{i+1}")
        tasks.append(
            cocotb.start_soon(
                axi_lite_transaction(
                    axi_masters[i],
                    memory_map[f"slave_{i}"] + 8,
                    wdata=test_write_data,
                    write=True,
                )
            )
        )
    for task in tasks:
        await Join(task)
    for i in range(0, n_masters):
        test_write_data = bytearray.fromhex(f"0000000{i+1}")
        assert axi_slaves[i].read(8, 4) == test_write_data
    await ClockCycles(clock, 2)

    dut._log.info(
        "Send a stream of write requests to the same slave"
    )

    tasks = []
    for i in range(0, n_masters):
        dut._log.info(f"Test write: m{i} -> s{i}")
        test_write_data = bytearray.fromhex(f"000000B{i+1}")
        tasks.append(
            cocotb.start_soon(
                subsequent_transactions(
                    2,
                    axi_master=axi_masters[i],
                    address=memory_map["slave_0"] + 12 + 4*i,
                    wdata=test_write_data,
                    write=True,
                )
            )
        )
    for task in tasks:
        await Join(task)

    for i in range(0, n_masters):
        test_write_data = bytearray.fromhex(f"000000B{i+1}")
        assert axi_slaves[0].read(12 + 4*i, 4) == test_write_data

    await ClockCycles(clock, 2)


async def subsequent_transactions(n_transactions, **kwargs):
    for _ in range(0, n_transactions):
        await cocotb.start_soon((axi_lite_transaction(**kwargs)))


async def axi_lite_transaction(
    axi_master: AxiLiteMaster,
    address: int,
    wdata: bytearray = bytearray(),
    read: bool = False,
    write: bool = False,
):
    if write:
        await with_timeout(
            axi_master.init_write(address, wdata).wait(),
            50,
            timeout_unit="step",
        )
    if read:
        await with_timeout(
            axi_master.init_read(address, 4).wait(), 50, timeout_unit="step"
        )
