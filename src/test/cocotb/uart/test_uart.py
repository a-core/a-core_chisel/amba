# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, with_timeout
import random

# pip install cocotbext-axi
from cocotbext.axi import AxiLiteMaster, AxiLiteBus
from cocotbext.uart import UartSink, UartSource

memory_map = {
    "tx_clk_thresh": 0x0000_0000,
    "tx_byte": 4,
    "tx_ctrl": 8,
    "tx_status": 12,
    "rx_clk_thresh": 16,
    "rx_byte": 20,
    "rx_status": 24,
}


def compose_word(data: str) -> bytearray:
    barray = bytearray()
    for char in data:
        ascii_val = ord(char)
        barray.append(ascii_val)
    return barray


async def print_uart_rx(uart_rx):
    global print_data
    print_data = ""
    while True:
        data = await uart_rx.read()
        print_data += data.decode("utf-8")


async def wait_for_done(axilite_master):
    while True:
        status_op = axilite_master.init_read(memory_map["tx_status"], 4)
        await with_timeout(status_op.wait(), 50, timeout_unit="us")
        # Loop while tx_busy bit is high
        if not (int.from_bytes(status_op.data.data, "little") & 1):
            break


@cocotb.test()
async def test_uart_rx(dut):
    n_samples = 10
    random.seed(69)
    test_data = [random.randint(0, 255) for _ in range(n_samples)]
    clk = dut.clock
    reset = dut.reset
    reset.value = 0
    axilite_master = AxiLiteMaster(
        AxiLiteBus.from_prefix(dut, "io_axi"), clk, reset, reset_active_level=True
    )
    uart_tx = UartSource(dut.io_uart_rx, baud=38400, bits=8)
    cocotb.start_soon(Clock(clk, 1, units="us").start())
    await ClockCycles(clk, 2)
    reset.value = 1
    await ClockCycles(clk, 2)
    reset.value = 0
    await ClockCycles(clk, 2)
    rx_clk_thresh = (26).to_bytes(4, byteorder="little")
    await with_timeout(
        axilite_master.init_write(memory_map["rx_clk_thresh"], rx_clk_thresh).wait(),
        50,
        timeout_unit="us",
    )
    for word in test_data:
        await uart_tx.write([word])
    result_data = []
    for word in test_data:
        data = axilite_master.init_read(memory_map["rx_byte"], length=1)
        await with_timeout(data.wait(), 500000, timeout_unit='us')
        result_data.append(int.from_bytes(data.data.data, byteorder='little'))
    await uart_tx.wait()
    assert_msg = "\nTest_data\t" + "\t".join(map(str, test_data)) + "\n"
    assert_msg += "Result_data\t" + "\t".join(map(str, result_data)) + "\n"
    assert test_data == result_data, assert_msg

@cocotb.test()
async def test_uart_tx(dut):
    clk = dut.clock
    reset = dut.reset
    reset.value = 0
    axilite_master = AxiLiteMaster(
        AxiLiteBus.from_prefix(dut, "io_axi"), clk, reset, reset_active_level=True
    )
    uart_rx = UartSink(dut.io_uart_tx, baud=38400, bits=8)
    cocotb.start_soon(Clock(clk, 1, units="us").start())
    cocotb.start_soon(print_uart_rx(uart_rx))
    await ClockCycles(clk, 2)
    reset.value = 1
    await ClockCycles(clk, 2)
    reset.value = 0
    await ClockCycles(clk, 2)

    dut._log.info("Test write to output register")
    tx_clk_thresh = (26).to_bytes(4, byteorder="little")
    await with_timeout(
        axilite_master.init_write(memory_map["tx_clk_thresh"], tx_clk_thresh).wait(),
        50,
        timeout_unit="us",
    )
    # Start with UART transmit disabled
    # (this is not necessary in a real application)
    await with_timeout(
        axilite_master.init_write(
            memory_map["tx_ctrl"], (0).to_bytes(4, byteorder="little")
        ).wait(),
        5000,
        timeout_unit="us",
    )

    test_string = "Hello World!"
    tx_test_bytes = compose_word(test_string)

    word_i = 0
    for tx_test_byte in tx_test_bytes:
        if word_i == 3:
            # Start transmit after some data has been written to the transmit FIFO
            await with_timeout(
                axilite_master.init_write(
                    memory_map["tx_ctrl"], (1).to_bytes(4, byteorder="little")
                ).wait(),
                5000,
                timeout_unit="us",
            )
        await with_timeout(
            axilite_master.init_write(
                memory_map["tx_byte"], tx_test_byte.to_bytes(4, byteorder="little")
            ).wait(),
            5000,
            timeout_unit="us",
        )
        word_i += 1

    await wait_for_done(axilite_master)

    print(print_data)
    assert print_data == test_string, "UART TX failed"
