// SPDX-License-Identifier: Apache-2.0

`default_nettype none

module AXI4LtoSRAMFormalProperties #(
    parameter ADDR_WIDTH=32,
    parameter DATA_WIDTH=32
) (
    /* AXI4-Lite Interface */
    // Clock and Reset
    input wire                    clk_rst_ACLK,
    input wire                    clk_rst_ARESETn,
    // Read Address Channel
    input  wire                    axi_arready,
    output wire                    axi_arvalid,
    output wire [  ADDR_WIDTH-1:0] axi_araddr,
    output wire [             2:0] axi_arprot,
    // Read Data Channel
    output wire                    axi_rready,
    input  wire                    axi_rvalid,
    input  wire [  DATA_WIDTH-1:0] axi_rdata,
    input  wire [             1:0] axi_rresp,
    // Write Address Channel
    input  wire                    axi_awready,
    output wire                    axi_awvalid,
    output wire [  ADDR_WIDTH-1:0] axi_awaddr,
    output wire [             2:0] axi_awprot,
    // Write Data Channel
    input  wire                    axi_wready,
    output wire                    axi_wvalid,
    output wire [  DATA_WIDTH-1:0] axi_wdata,
    output wire [DATA_WIDTH/8-1:0] axi_wstrb,
    // Write Response Channel
    output wire                    axi_bready,
    input  wire                    axi_bvalid,
    input  wire [             1:0] axi_bresp,

    input wire sram_read_req,
    output wire sram_read_gnt,
    output wire sram_read_fault,
    output wire sram_read_data_valid,
    input wire[ADDR_WIDTH-1:0] sram_read_addr,
    output wire[DATA_WIDTH-1:0] sram_read_data_bits,
    input wire sram_write_req,
    output wire sram_write_gnt,
    output wire sram_write_fault,
    input wire[ADDR_WIDTH-1:0] sram_write_addr,
    input wire[DATA_WIDTH/8-1:0] sram_write_mask,
    input wire[DATA_WIDTH-1:0] sram_write_data

);

    // assert reset on first cycle
    always @*
        if ($initstate)
            assume(!clk_rst_ARESETn);
        else
            assume(clk_rst_ARESETn);
    
    /* AXI4-Lite Master Formal Assumptions */
    // These constrain the master port to only issue AXI4-Lite conformant transactions.
    // This is mainly useful for verifying interconnect where slave-side assertions depend
    // directly on the master outputs as the interconnect simply reroutes the signals to
    // another interface.

    // During reset a master interface must drive axi_arvalid, axi_awvalid, and axi_wvalid low. (A3-40)
    // FIXME: it seems like chisel doesn't yet support active low async resets, using sync
    //        resets instead
    always @(posedge clk_rst_ACLK)
        if (!clk_rst_ARESETn) begin
            assume(!axi_arvalid);
            assume(!axi_awvalid);
            assume(!axi_wvalid);
        end

    // The earliest point after reset that a master is permitted to begin driving axi_arvalid, axi_awvalid,
    // or axi_wvalid high is at a rising clk_rst_ACLK edge after clk_rst_ARESETn is high (example figure). (A3-40)
    always @(posedge clk_rst_ACLK)
        // assert that valid doesn't go high at the edge clk_rst_ARESETn is deasserted
        // i.e. assume that valid is low during the cycle after reset
        if (!$initstate && clk_rst_ARESETn && $past(!clk_rst_ARESETn)) begin
            assume(!axi_arvalid);
            assume(!axi_awvalid);
            assume(!axi_wvalid);
        end
    
    // When asserted, axi_awvalid must remain asserted until the rising clock edge after the slave 
    // asserts axi_awready. (A3-42)
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_awvalid && !axi_awready))
            assume(axi_awvalid);

    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_awvalid && !axi_awready)) begin
            assume($stable(axi_awaddr));
            assume($stable(axi_awprot));
        end
    
    // When asserted, axi_wvalid must remain asserted until the rising clock edge after the slave 
    // asserts axi_wready. (A3-43)
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_wvalid && !axi_wready))
            assume(axi_wvalid);
    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_wvalid && !axi_wready)) begin
            assume($stable(axi_wdata));
            assume($stable(axi_wstrb));
        end

    // When asserted, axi_arvalid must remain asserted until the rising clock edge after the slave 
    // asserts the axi_awready signal. (A3-43)
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_arvalid && !axi_awready))
            assume(axi_arvalid);
    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn && axi_arvalid && !axi_awready)) begin
            assume($stable(axi_araddr));
            assume($stable(axi_arprot));
        end

    // Assume that sram_rdata stays the same if address was the same
    always @(posedge clk_rst_ACLK)
        if ($stable(sram_read_addr))
            assume($stable(sram_read_data_bits));

    // If write channel handshake occurs, sram_wen should be high and sram_wdata should be
    // the same as axi_wdata and sram_waddr should be same as axi_awaddr
    always @(posedge clk_rst_ACLK)
        if (!$initstate && clk_rst_ARESETn && axi_awvalid && axi_awready && axi_wvalid && axi_wready && $past(axi_bready)) begin
            assert(sram_write_req);
            assert(sram_write_addr == axi_awaddr);
            assert(sram_write_data == axi_wdata);
        end

    /* Cover statements to ensure that it's possible to reach transfer condition on all channels */
    always @(posedge clk_rst_ACLK)
        if (!$initstate && $past(clk_rst_ARESETn)) begin
            cover(axi_arready && axi_arvalid);
            cover(axi_rready && axi_rvalid);
            cover(axi_awready && axi_awvalid);
            cover(axi_wready && axi_wvalid);
            cover(axi_wvalid && !axi_wready);
            cover(axi_bready && axi_bvalid);
        end


endmodule
