// SPDX-License-Identifier: Apache-2.0

`default_nettype none

module AXI4LSlaveFormalProperties #(
    parameter ADDR_WIDTH=32,
    parameter DATA_WIDTH=32
) (
    /* AXI4-Lite Interface */
    // Clock and Reset
    input  wire                    ACLK,
    input  wire                    ARESETn,
    // Read Address Channel
    output wire                    ARREADY,
    input  wire                    ARVALID,
    input  wire [  ADDR_WIDTH-1:0] ARADDR,
    input  wire [             2:0] ARPROT,
    // Read Data Channel
    input  wire                    RREADY,
    output wire                    RVALID,
    output wire [  DATA_WIDTH-1:0] RDATA,
    output wire [             1:0] RRESP,
    // Write Address Channel
    output wire                    AWREADY,
    input  wire                    AWVALID,
    input  wire [  ADDR_WIDTH-1:0] AWADDR,
    input  wire [             2:0] AWPROT,
    // Write Data Channel
    output wire                    WREADY,
    input  wire                    WVALID,
    input  wire [  DATA_WIDTH-1:0] WDATA,
    input  wire [DATA_WIDTH/8-1:0] WSTRB,
    // Write Response Channel
    input  wire                    BREADY,
    output wire                    BVALID,
    output wire [             1:0] BRESP
);

    /* AXI4-Lite Slave Formal Assertions */

    // During reset a master interface must drive ARVALID, AWVALID, and WVALID low. (A3-40)
    // FIXME: it seems like chisel doesn't yet support active low async resets, using sync
    //        resets instead
    always @(posedge ACLK)
        if (!$initstate && $past(!ARESETn)) begin
            assert(!ARVALID);
            assert(!AWVALID);
            assert(!WVALID);
        end

    // The earliest point after reset that a master is permitted to begin driving ARVALID, AWVALID,
    // or WVALID high is at a rising ACLK edge after ARESETn is high (example figure). (A3-40)
    always @(posedge ACLK)
        // assert that valid doesn't go high at the edge ARESETn is deasserted
        if (!$initstate && $rose(ARESETn)) begin
            assert(!ARVALID);
            assert(!AWVALID);
            assert(!WVALID);
        end
    
    // When asserted, AWVALID must remain asserted until the rising clock edge after the slave 
    // asserts AWREADY. (A3-42)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && AWVALID) && !AWVALID)
            assert($past(AWREADY));
    
    // When asserted, WVALID must remain asserted until the rising clock edge after the slave 
    // asserts WREADY. (A3-43)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && WVALID) && !WVALID)
            assert($past(WREADY));

    // When asserted, ARVALID must remain asserted until the rising clock edge after the slave 
    // asserts the ARREADY signal. (A3-43)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && ARVALID) && !ARVALID)
            assert($past(ARREADY));

    /* Cover statements to ensure that it's possible to reach transfer condition on all channels */
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn)) begin
            cover(ARREADY && ARVALID);
            cover(RREADY && RVALID);
            cover(AWREADY && AWVALID);
            cover(WREADY && WVALID);
            cover(BREADY && BVALID);
        end


endmodule
