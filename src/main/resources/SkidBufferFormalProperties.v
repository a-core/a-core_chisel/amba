// SPDX-License-Identifier: Apache-2.0

`default_nettype none

module SkidBufferFormalProperties(
  input         clock,
  input         reset,
  input         buffer_in_ready,
  input         buffer_in_valid,
  input  [31:0] buffer_in_bits,
  input         buffer_out_ready,
  input         buffer_out_valid,
  input  [31:0] buffer_out_bits
);

    // Assert reset on first cycle
    always @*
        if ($initstate) begin
            assume(reset);
            assume(~buffer_in_valid);
        end else
            assume(~reset);

    // Assume that input data remains the same if buffer is full
    always @(posedge clock)
        if (!$initstate && buffer_in_valid && !buffer_in_ready)
            assume(buffer_in_valid && $stable(buffer_in_bits));

    // Once out_valid goes high, the data cannot change until the clock after out_ready
    always @(posedge clock)
        if (!$initstate && $past(!reset && buffer_out_valid && !buffer_out_ready))
            assert(buffer_out_valid && $stable(buffer_out_bits));

    // If, on the last clock cycle, input was valid, skid buffer was ready, but output was not,
    // buffer stores data and is not ready on the next clock
    // Output must be valid
    always @(posedge clock)
        if (!$initstate && $past(!reset && buffer_in_valid && buffer_in_ready && !buffer_out_ready)) begin
            assert(!buffer_in_ready);
            assert(buffer_out_valid);
        end

    // If output is ready and in_valid is high, out_valid should be high
    always @(posedge clock)
        if (!$initstate && !reset && buffer_out_ready && buffer_in_valid)
            assert(buffer_out_valid);

    // If input is not valid, output is ready and buffer is empty, out valid should be low
    always @(posedge clock)
        if (!$initstate && !reset && !buffer_in_valid && buffer_in_ready && buffer_out_ready)
            assert(!buffer_out_valid);

    // If buffer was full and output was not ready, buffer is still full
    always @(posedge clock)
        if (!$initstate && $past(!reset && !buffer_in_ready && !buffer_out_ready))
            assert(!buffer_in_ready);


endmodule