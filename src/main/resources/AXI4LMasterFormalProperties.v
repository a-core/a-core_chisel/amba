// SPDX-License-Identifier: Apache-2.0

`default_nettype none

module AXI4LMasterFormalProperties #(
    parameter ADDR_WIDTH=32,
    parameter DATA_WIDTH=32
) (
    /* AXI4-Lite Interface */
    // Clock and Reset
    input wire                    ACLK,
    input wire                    ARESETn,
    // Read Address Channel
    input  wire                    ARREADY,
    output wire                    ARVALID,
    output wire [  ADDR_WIDTH-1:0] ARADDR,
    output wire [             2:0] ARPROT,
    // Read Data Channel
    output wire                    RREADY,
    input  wire                    RVALID,
    input  wire [  DATA_WIDTH-1:0] RDATA,
    input  wire [             1:0] RRESP,
    // Write Address Channel
    input  wire                    AWREADY,
    output wire                    AWVALID,
    output wire [  ADDR_WIDTH-1:0] AWADDR,
    output wire [             2:0] AWPROT,
    // Write Data Channel
    input  wire                    WREADY,
    output wire                    WVALID,
    output wire [  DATA_WIDTH-1:0] WDATA,
    output wire [DATA_WIDTH/8-1:0] WSTRB,
    // Write Response Channel
    output wire                    BREADY,
    input  wire                    BVALID,
    input  wire [             1:0] BRESP
);

    // assert reset on first cycle
    always @*
        if ($initstate)
            assume(!ARESETn);
        else
            assume(ARESETn);

    /* AXI4-Lite Master Formal Assumptions */
    // These constrain the master port to only issue AXI4-Lite conformant transactions.
    // This is mainly useful for verifying interconnect where slave-side assertions depend
    // directly on the master outputs as the interconnect simply reroutes the signals to
    // another interface.

    // During reset a master interface must drive ARVALID, AWVALID, and WVALID low. (A3-40)
    // FIXME: it seems like chisel doesn't yet support active low async resets, using sync
    //        resets instead
    always @(posedge ACLK)
        if ($past(!ARESETn)) begin
            assume(!ARVALID);
            assume(!AWVALID);
            assume(!WVALID);
        end

    // The earliest point after reset that a master is permitted to begin driving ARVALID, AWVALID,
    // or WVALID high is at a rising ACLK edge after ARESETn is high (example figure). (A3-40)
    always @(posedge ACLK)
        // assert that valid doesn't go high at the edge ARESETn is deasserted
        // i.e. assume that valid is low during the cycle after reset
        if (!$initstate && ARESETn && $past(!ARESETn)) begin
            assume(!ARVALID);
            assume(!AWVALID);
            assume(!WVALID);
        end
    
    // When asserted, AWVALID must remain asserted until the rising clock edge after the slave 
    // asserts AWREADY. (A3-42)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && AWVALID && !AWREADY))
            assume(AWVALID);
    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && AWVALID && !AWREADY)) begin
            assume($stable(AWADDR));
            assume($stable(AWPROT));
        end

    // When asserted, WVALID must remain asserted until the rising clock edge after the slave 
    // asserts WREADY. (A3-43)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && WVALID && !WREADY))
            assume(WVALID);
    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && WVALID && !WREADY)) begin
            assume($stable(WDATA));
            assume($stable(WSTRB));
        end

    // When asserted, ARVALID must remain asserted until the rising clock edge after the slave 
    // asserts the ARREADY signal. (A3-43)
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && ARVALID && !ARREADY))
            assume(ARVALID);

    // Channel signaling should not change before transfer after *VALID has been asserted
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && ARVALID && !ARREADY)) begin
            assume($stable(ARADDR));
            assume($stable(ARPROT));
        end

    // Do not write to the same place that is being read
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn) && (ARVALID || RVALID)) begin
            assume(AWADDR[31:2] != ARADDR[31:2]);
        end

    // Read data should remain constant until handshake occurs
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && !RREADY && RVALID)) begin
            assert(RVALID);
            assert($stable(RDATA));
        end

    // Write response data should remain constant until handshake occurs
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn && !BREADY && BVALID)) begin
            assert(BVALID);
            assert($stable(BRESP));
        end

    /* Cover statements to ensure that it's possible to reach transfer condition on all channels */
    always @(posedge ACLK)
        if (!$initstate && $past(ARESETn)) begin
            cover(ARREADY && ARVALID);
            cover(RREADY && RVALID);
            cover(AWREADY && AWVALID);
            cover(WREADY && WVALID);
            cover(BREADY && BVALID);
        end


endmodule
