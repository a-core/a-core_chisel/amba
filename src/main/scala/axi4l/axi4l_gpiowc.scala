// SPDX-License-Identifier: Apache-2.0

// Chisel module AXI4-lite GPIO with control registers, for data direction and pullup/down control of bidirectional IO cells
// Inititally written by Otto Simola otto.simola@aalto.fi, 2023-06-05

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import amba.common._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

class GPIOWC(inout_width: Int, withSync: Boolean = true) extends Module {
  val io = IO(new Bundle {
    val external = new Bundle {
      // Inputs coming from outside world
      val in_vec = Input(UInt(inout_width.W))
      // Outputs towards outside world
      val out_vec = Output(UInt(inout_width.W))
      // Outputs towards outside world
      val puen_vec = Output(UInt(inout_width.W))
      // Outputs towards outside world
      val pden_vec = Output(UInt(inout_width.W))
      // Outputs towards outside world
      val rxen_vec = Output(UInt(inout_width.W))
      // Outputs towards outside world
      val trien_vec = Output(UInt(inout_width.W))
    }
    val internal = new Bundle {
      // Synchronized input data towards the processor
      val in_data = Output(UInt(inout_width.W))
      // Output data from the processor
      val out_data = Input(UInt(inout_width.W))
      // Output data from the processor
      val puen_data = Input(UInt(inout_width.W))
      // Output data from the processor
      val pden_data = Input(UInt(inout_width.W))
      // Output data from the processor
      val rxen_data = Input(UInt(inout_width.W))
      // Output data from the processor
      val trien_data = Input(UInt(inout_width.W))
    }
  })

  // Optional synchronizer register for input data
  val in_reg_cdc_reg = if (withSync) RegNext(io.external.in_vec) else RegInit(0.U(inout_width.W))
  val in_reg         = if (withSync) RegNext(in_reg_cdc_reg) else RegNext(io.external.in_vec)

  // Output data simply fed through
  io.external.out_vec   := io.internal.out_data
  io.external.puen_vec  := io.internal.puen_data
  io.external.pden_vec  := io.internal.pden_data
  io.external.rxen_vec  := io.internal.rxen_data
  io.external.trien_vec := io.internal.trien_data
  io.internal.in_data   := in_reg
}

class AXI4LGPIOWC(
    val addr_width: Int = 32,
    val data_width: Int = 32,
    withSync: Boolean = true,
    inout_width: Int = 32
) extends RawModule
    with AXI4LSlaveLogic with AXI4LSlave {

  val gpiowc = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn){ Module(new GPIOWC(inout_width, withSync)) }
  val io   = IO(chiselTypeOf(gpiowc.io.external))

  // Memory map for input and output registers
  val memory_map = Map[Data, MemoryRange](
    gpiowc.io.internal.in_data    -> MemoryRange(begin = "h0000_0000", end = "h0000_000F", mode = MemoryMode.R, name = "IN_REG"),
    gpiowc.io.internal.out_data   -> MemoryRange(begin = "h0000_0010", end = "h0000_001F", mode = MemoryMode.RW, name = "OUT_REG"),
    gpiowc.io.internal.puen_data  -> MemoryRange(begin = "h0000_0020", end = "h0000_002F", mode = MemoryMode.RW, name = "PUEN_REG"),
    gpiowc.io.internal.pden_data  -> MemoryRange(begin = "h0000_0030", end = "h0000_003F", mode = MemoryMode.RW, name = "PDEN_REG"),
    gpiowc.io.internal.rxen_data  -> MemoryRange(begin = "h0000_0040", end = "h0000_004F", mode = MemoryMode.RW, name = "RXEN_REG"),
    gpiowc.io.internal.trien_data -> MemoryRange(begin = "h0000_0050", end = "h0000_005F", mode = MemoryMode.RW, name = "TRIEN_REG")
  )
  buildAxi4LiteSlaveLogic()
  io <> gpiowc.io.external
}

/** Test wrapper for AXI4LGPIOWC. Renames AXI4L signals to standard format.
  * @param addr_width
  *   Address bus width
  * @param data_width
  *   Data bus width
  * @param XLEN
  *   Global register width
  */
class AXI4LGPIOWCTestWrapper(addr_width: Int, data_width: Int, withSync: Boolean = true, inout_width: Int = 32) extends RawModule {
  val axi4gpiowc = Module(new AXI4LGPIOWC(addr_width, data_width, true, inout_width))

  val io_io = IO(new Bundle {
    val in_vec  = Input(UInt(inout_width.W))
    val out_vec = Output(UInt(inout_width.W))
    val puen_vec = Output(UInt(inout_width.W))
    val pden_vec = Output(UInt(inout_width.W))
    val rxen_vec = Output(UInt(inout_width.W))
    val trien_vec = Output(UInt(inout_width.W))
  })

  val axi_io = IO(new Bundle {
    // Clock and Reset
    val aclk    = Input(Clock())
    val aresetn = Input(Bool())
    // Read Address Channel
    val arready = Output(Bool())
    val arvalid = Input(Bool())
    val araddr  = Input(UInt(addr_width.W))
    val arprot  = Input(UInt(3.W))
    // Read Data Channel
    val rready = Input(Bool())
    val rvalid = Output(Bool())
    val rdata  = Output(UInt(data_width.W))
    val rresp  = Output(Response())
    // Write Address Channel
    val awready = Output(Bool())
    val awvalid = Input(Bool())
    val awaddr  = Input(UInt(addr_width.W))
    val awprot  = Input(UInt(3.W))
    // Write Data Channel
    val wready = Output(Bool())
    val wvalid = Input(Bool())
    val wdata  = Input(UInt(data_width.W))
    val wstrb  = Input(UInt((data_width / 8).W))
    // Write Response Channel
    val bready = Input(Bool())
    val bvalid = Output(Bool())
    val bresp  = Output(Response())
  })

  axi_io.arready := axi4gpiowc.slave_port.AR.ready
  axi_io.rvalid  := axi4gpiowc.slave_port.R.valid
  axi_io.rdata   := axi4gpiowc.slave_port.R.bits.DATA
  axi_io.rresp   := axi4gpiowc.slave_port.R.bits.RESP
  axi_io.awready := axi4gpiowc.slave_port.AW.ready
  axi_io.wready  := axi4gpiowc.slave_port.W.ready
  axi_io.bvalid  := axi4gpiowc.slave_port.B.valid
  axi_io.bresp   := axi4gpiowc.slave_port.B.bits.RESP

  axi4gpiowc.clk_rst.ACLK            := axi_io.aclk
  axi4gpiowc.clk_rst.ARESETn         := axi_io.aresetn
  axi4gpiowc.slave_port.AR.valid     := axi_io.arvalid
  axi4gpiowc.slave_port.AR.bits.ADDR := axi_io.araddr
  axi4gpiowc.slave_port.AR.bits.PROT := axi_io.arprot
  axi4gpiowc.slave_port.R.ready      := axi_io.rready
  axi4gpiowc.slave_port.AW.valid     := axi_io.awvalid
  axi4gpiowc.slave_port.AW.bits.ADDR := axi_io.awaddr
  axi4gpiowc.slave_port.AW.bits.PROT := axi_io.awprot
  axi4gpiowc.slave_port.W.valid      := axi_io.wvalid
  axi4gpiowc.slave_port.W.bits.DATA  := axi_io.wdata
  axi4gpiowc.slave_port.W.bits.STRB  := axi_io.wstrb
  axi4gpiowc.slave_port.B.ready      := axi_io.bready

  io_io.out_vec       := axi4gpiowc.io.out_vec
  io_io.puen_vec    := axi4gpiowc.io.puen_vec
  io_io.pden_vec  := axi4gpiowc.io.pden_vec
  io_io.rxen_vec   := axi4gpiowc.io.rxen_vec
  io_io.trien_vec    := axi4gpiowc.io.trien_vec
  axi4gpiowc.io.in_vec  := io_io.in_vec
}

object AXI4LGPIOWCTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LGPIOWCTestWrapper(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}
