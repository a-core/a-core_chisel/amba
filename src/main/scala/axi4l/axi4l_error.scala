// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import amba.common.SkidBuffer
import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** Error slave that produces an error for all accesses.
 *  This is used to trigger address decode errors.
*/
class AXI4LError(addr_width: Int, data_width: Int) 
  extends RawModule with AXI4LSlave{
  // AXI4-Lite signaling
  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {
    val sram_iface = Module(new AXI4LtoSRAM(addr_width, data_width))
    sram_iface.clk_rst    <> clk_rst
    sram_iface.slave_port <> slave_port

    sram_iface.io.read.gnt        := sram_iface.io.read.req
    sram_iface.io.read.data.bits  := 0.U
    sram_iface.io.read.data.valid := RegNext(sram_iface.io.read.req)
    sram_iface.io.read.fault      := true.B

    sram_iface.io.write.gnt   := sram_iface.io.write.req
    sram_iface.io.write.fault := true.B
  }
}

object AXI4LError extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LError(addr_width=32, data_width=32)))
  (new ChiselStage).execute(args, annos)
}