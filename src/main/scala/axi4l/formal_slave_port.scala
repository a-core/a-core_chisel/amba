// SPDX-License-Identifier: Apache-2.0

package amba.axi4l.formal

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.axi4l._
import amba.common._

/** Mock AXI4-Lite slave device containing formal properties */
class AXI4LFormalSlave(
    addr_width: Int = 32,
    data_width: Int = 32
) extends RawModule with AXI4LSlave {
  val clk_rst    = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width = 32, data_width = 32))
  val properties = Module(new AXI4LSlaveFormalProperties(addr_width, data_width))
  dontTouch(slave_port)

  // AXI Clock and Reset
  properties.io.ACLK    := clk_rst.ACLK
  properties.io.ARESETn := clk_rst.ARESETn
  // Read Address Channel
  slave_port.AR.ready   := properties.io.ARREADY
  properties.io.ARVALID := slave_port.AR.valid
  properties.io.ARADDR  := slave_port.AR.bits.ADDR
  properties.io.ARPROT  := slave_port.AR.bits.PROT
  // Read Data Channel
  properties.io.RREADY   := slave_port.R.ready
  slave_port.R.valid     := properties.io.RVALID
  slave_port.R.bits.DATA := properties.io.RDATA
  slave_port.R.bits.RESP := Response(properties.io.RRESP)
  // Write Address Channel
  slave_port.AW.ready   := properties.io.AWREADY
  properties.io.AWVALID := slave_port.AW.valid
  properties.io.AWADDR  := slave_port.AW.bits.ADDR
  properties.io.AWPROT  := slave_port.AW.bits.PROT
  // Write Data Channel
  slave_port.W.ready   := properties.io.WREADY
  properties.io.WVALID := slave_port.W.valid
  properties.io.WVALID := slave_port.W.valid
  properties.io.WDATA  := slave_port.W.bits.DATA
  properties.io.WSTRB  := slave_port.W.bits.STRB
  // Write Response Channel
  properties.io.BREADY   := slave_port.B.ready
  slave_port.B.valid     := properties.io.BVALID
  slave_port.B.bits.RESP := Response(properties.io.BRESP)
}

/** Generate Verilog */
object AXI4LFormalSlave extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LFormalSlave(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}

/** BlackBox of the AXI4-Lite slave port formal assertions written in verilog */
class AXI4LSlaveFormalProperties(
    addr_width: Int,
    data_width: Int
) extends BlackBox(Map("ADDR_WIDTH" -> addr_width, "DATA_WIDTH" -> data_width)) with HasBlackBoxResource {

  val io = IO(new Bundle {
    // Clock and Reset
    val ACLK    = Input(Clock())
    val ARESETn = Input(Bool())
    // Read Address Channel
    val ARREADY = Output(Bool())
    val ARVALID = Input(Bool())
    val ARADDR  = Input(UInt(addr_width.W))
    val ARPROT  = Input(UInt(3.W))
    // Read Data Channel
    val RREADY = Input(Bool())
    val RVALID = Output(Bool())
    val RDATA  = Output(UInt(data_width.W))
    val RRESP  = Output(UInt(2.W))
    // Write Address Channel
    val AWREADY = Output(Bool())
    val AWVALID = Input(Bool())
    val AWADDR  = Input(UInt(addr_width.W))
    val AWPROT  = Input(UInt(3.W))
    // Write Data Channel
    val WREADY = Output(Bool())
    val WVALID = Input(Bool())
    val WDATA  = Input(UInt(data_width.W))
    val WSTRB  = Input(UInt((data_width / 8).W))
    // Write Response Channel
    val BREADY = Input(Bool())
    val BVALID = Output(Bool())
    val BRESP  = Output(UInt(2.W))
  })

  addResource("/AXI4LSlaveFormalProperties.v")
}
