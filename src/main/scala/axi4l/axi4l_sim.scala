// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}


/** Serial character stream for printing to STDOUT in simulations
  * Writes the least significant byte of WDATA to STDOUT using verilog fwrite.
  * @param addr_width AXI4-Lite address bus with.
  * @param data_width AXI4-Lite data bus with.
  */
class AXI4LPrinter(addr_width: Int, data_width: Int) extends RawModule with AXI4LSlave {
  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {
    // convert from axi4l to sram signaling
    val sram_iface = Module(new AXI4LtoSRAM(addr_width, data_width))
    sram_iface.clk_rst <> clk_rst
    sram_iface.slave_port <> slave_port
    sram_iface.io.read.data.bits := DontCare
    sram_iface.io.read.gnt := sram_iface.io.read.req
    sram_iface.io.read.fault := true.B

    sram_iface.io.write.gnt   := sram_iface.io.write.req
    sram_iface.io.write.fault := false.B
    // print out write data in simulation
    when (sram_iface.io.write.req) {
      printf("%c", sram_iface.io.write.data(7,0))
    }
  }
}

object AXI4LPrinter extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LPrinter(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}
