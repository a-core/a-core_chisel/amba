// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.common._
import amba.axi4l.formal._

/** AXI4-Lite address decoder Maps an AXI4-Lite address to a slave index and the corresponding slave local address.
  * Defaults to index 0 if there was no match.
  * @param addr_width
  *   Width of the axi4l address bus
  * @param memory_map
  *   Map of memory ranges to corresponding axi4l slave module instances
  */
class AddressDecoder(addr_width: Int, memory_map: Seq[MemoryRange]) extends Module {

  val io = IO(new Bundle {
    // inputs
    val en   = Input(Bool())
    val addr = Input(UInt(addr_width.W))

    // outputs
    val addr_local = Output(UInt(addr_width.W))
    val index      = Output(UInt(log2Ceil(memory_map.size).W))
    val valid      = Output(Bool())
  })

  val addr     = Mux(io.en, io.addr, 0.U)
  val baseAddr = WireDefault(0.U)
  io.index      := 0.U
  io.valid      := io.en
  io.addr_local := addr - baseAddr

  for ((mappings, i) <- memory_map.zipWithIndex) {
    val range = mappings
    when(addr >= range.begin.U && addr <= range.end.U) {
      io.index  := i.U
      baseAddr  := range.begin.U
    }
  }
}

/**
  * AXI4-Lite Demultiplexer with integrated address decoders.
  *
  * @param addr_width
  * @param data_width
  * @param mem_ranges
  */
class AXI4LMMDemux(addr_width: Int, data_width: Int, mem_ranges: Seq[MemoryRange]) extends RawModule
    with AXI4LSlave {
  val clk_rst      = IO(new AXI4LClkRst)
  val slave_port   = IO(new AXI4LIO(addr_width, data_width))
  val master_ports = IO(Vec(mem_ranges.size, Flipped(new AXI4LIO(addr_width, data_width))))

  val waddr_decoder = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn.asBool) {
    Module(new AddressDecoder(addr_width, mem_ranges))
  }
  val raddr_decoder = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn.asBool) {
    Module(new AddressDecoder(addr_width, mem_ranges))
  }
  val demux         = Module(new AXI4LMMDemuxUnit(addr_width, data_width, mem_ranges.size))

  waddr_decoder.io.en   := true.B
  raddr_decoder.io.en   := true.B
  waddr_decoder.io.addr := slave_port.AW.bits.ADDR
  raddr_decoder.io.addr := slave_port.AR.bits.ADDR

  demux.clk_rst      <> clk_rst
  demux.master_ports <> master_ports
  demux.slave_port   <> slave_port
  demux.slave_port.AR.bits.ADDR := raddr_decoder.io.addr_local
  demux.slave_port.AW.bits.ADDR := waddr_decoder.io.addr_local
  demux.r_idx                   := raddr_decoder.io.index
  demux.w_idx                   := waddr_decoder.io.index

}

/**
  * AXI4-Lite demultiplexer. 1 master to n slaves.
  *
  * @param addr_width
  * @param data_width
  * @param n_slaves
  */
class AXI4LMMDemuxUnit(addr_width: Int, data_width: Int, n_slaves: Int) extends RawModule
    with AXI4LSlave {

  val indexWidth = log2Ceil(n_slaves)
  val clk_rst      = IO(new AXI4LClkRst)
  val slave_port   = IO(new AXI4LIO(addr_width, data_width))
  val w_idx        = IO(Input(UInt(indexWidth.W)))
  val r_idx        = IO(Input(UInt(indexWidth.W)))
  val master_ports = IO(Vec(n_slaves, Flipped(new AXI4LIO(addr_width, data_width))))


  withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {

    /* -------------------- Skid buffers  ----------------- */
    // These modules let data through by default, but if there
    // is a case where in.valid == true and out.ready == false,
    // the data is stored into a buffer register.
    // This allows a full-throughput interconnect while still having
    // decoupled ready and valid signals (i.e. not combinationally dependent)

    val ar_buf = Module(new SkidBuffer(new Bundle {
      val bits = new AR(addr_width)
      val index = UInt(indexWidth.W)
    }))
    val r_buf = Module(new SkidBuffer(new R(data_width)))
    val aw_buf = Module(new SkidBuffer(new Bundle {
      val bits  = new AW(addr_width)
      val index = UInt(indexWidth.W)
    }))
    val w_buf = Module(new SkidBuffer(new W(data_width)))
    val b_buf = Module(new SkidBuffer(new B))

    /* -------------------- Addr decoders ----------------- */

    /* --------------------    FIFOs   -------------------- */
    // FIFOs store the destination port. Thus, AW channel can be freed once a valid
    // address has been sent, since the target slave is stored in wQueue and bQueue.
    // Same for AR channel.
    // ``flow = true`` means that enq.valid traverses to deq.valid combinationally.
    // It is enabled for wQueue so that AWVALID and WVALID assert simultaneously.
    // This reduces latency by 1 clock cycle.
    // For rQueue and bQueue it is disabled as it shortens combinational path
    // and latency is unaffected.

    // Write data FIFO
    val wQueue = Module(new Queue(UInt(indexWidth.W), 4, flow = true))
    val wQueueFull       = !wQueue.io.enq.ready
    val wQueueEmpty      = !wQueue.io.deq.valid
    wQueue.io.enq.bits  := aw_buf.io.out.bits.index
    wQueue.io.enq.valid := false.B
    wQueue.io.deq.ready := false.B

    // Write response data FIFO
    val bQueue = Module(new Queue(UInt(indexWidth.W), 4, flow = false))
    val bQueueFull       = !bQueue.io.enq.ready
    val bQueueEmpty      = !bQueue.io.deq.valid
    bQueue.io.enq.bits  := wQueue.io.deq.bits
    bQueue.io.enq.valid := wQueue.io.deq.valid && wQueue.io.deq.ready
    bQueue.io.deq.ready := false.B

    // Read data FIFO
    val rQueue = Module(new Queue(UInt(indexWidth.W), 4, flow = false))
    val rQueueFull       = !rQueue.io.enq.ready
    val rQueueEmpty      = !rQueue.io.deq.valid
    rQueue.io.enq.bits  := ar_buf.io.out.bits.index
    rQueue.io.enq.valid := false.B
    rQueue.io.deq.ready := false.B

    /* -------------------- AW channel -------------------- */

    aw_buf.io.in.bits.bits      := slave_port.AW.bits
    aw_buf.io.in.bits.index     := w_idx
    aw_buf.io.in.valid          := slave_port.AW.valid
    slave_port.AW.ready         := aw_buf.io.in.ready
    aw_buf.io.out.ready         := false.B

    // master port AW output defaults
    for (i <- 0 until n_slaves) {
      master_ports(i).AW.valid     := false.B
      master_ports(i).AW.bits      := aw_buf.io.out.bits.bits
    }

    // Lock AW channel if target slave is not ready or fifo is full
    val lockAwValid = RegInit(false.B)

    when (lockAwValid) {
      // Handle unexpected case where lockAwValid is high and aw_buf.io.out.valid is low
      // by deasserting lockAwValid
      master_ports(aw_buf.io.out.bits.index).AW.valid := aw_buf.io.out.valid
      when (master_ports(aw_buf.io.out.bits.index).AW.ready || !aw_buf.io.out.valid) {
        aw_buf.io.out.ready := aw_buf.io.out.valid
        lockAwValid := false.B
      }
    } .elsewhen (!wQueueFull && aw_buf.io.out.valid) {
      wQueue.io.enq.valid := true.B
      master_ports(aw_buf.io.out.bits.index).AW.valid := true.B
      when (master_ports(aw_buf.io.out.bits.index).AW.ready) {
        aw_buf.io.out.ready := true.B
      } .otherwise {
        aw_buf.io.out.ready := false.B
        lockAwValid         := true.B
      }
    }

    /* --------------------  W channel -------------------- */

    w_buf.io.in <> slave_port.W

    // master port W output defaults
    for (i <- 0 until n_slaves) {
      master_ports(i).W.valid := !wQueueEmpty && !bQueueFull && w_buf.io.out.valid && (wQueue.io.deq.bits === i.U)
      master_ports(i).W.bits  := w_buf.io.out.bits
    }

    w_buf.io.out.ready  := !wQueueEmpty && !bQueueFull && master_ports(wQueue.io.deq.bits).W.ready
    wQueue.io.deq.ready := w_buf.io.out.valid && w_buf.io.out.ready

    /* --------------------  B channel -------------------- */

    b_buf.io.out <> slave_port.B

    // master port B output defaults
    for (i <- 0 until n_slaves) {
      master_ports(i).B.ready := !bQueueEmpty && b_buf.io.in.ready && (bQueue.io.deq.bits === i.U)
    }

    b_buf.io.in.valid     := !bQueueEmpty && master_ports(bQueue.io.deq.bits).B.valid
    b_buf.io.in.bits.RESP := Mux(bQueueEmpty, Response.SLVERR, master_ports(bQueue.io.deq.bits).B.bits.RESP)

    bQueue.io.deq.ready := b_buf.io.in.valid && b_buf.io.in.ready

    /* -------------------- AR channel -------------------- */

    ar_buf.io.in.bits.bits      := slave_port.AR.bits
    ar_buf.io.in.bits.index     := r_idx
    ar_buf.io.in.valid          := slave_port.AR.valid
    slave_port.AR.ready         := ar_buf.io.in.ready

    // master port AR output defaults
    for (i <- 0 until n_slaves) {
      master_ports(i).AR.bits  := ar_buf.io.out.bits.bits
      master_ports(i).AR.valid := !rQueueFull && ar_buf.io.out.valid && (ar_buf.io.out.bits.index === i.U)
    }
    ar_buf.io.out.ready := !rQueueFull && master_ports(ar_buf.io.out.bits.index).AR.ready
    rQueue.io.enq.valid := ar_buf.io.out.valid && ar_buf.io.out.ready

    /* --------------------  R channel -------------------- */

    r_buf.io.out <> slave_port.R

    // master port R output defaults
    for (i <- 0 until n_slaves) {
      master_ports(i).R.ready := !rQueueEmpty && r_buf.io.in.ready && (rQueue.io.deq.bits === i.U)
    }

    val zeroReadBundle = (new R(data_width)).Lit(_.DATA -> 0.U, _.RESP -> Response.SLVERR)

    r_buf.io.in.bits  := Mux(rQueueEmpty, zeroReadBundle, master_ports(rQueue.io.deq.bits).R.bits)
    r_buf.io.in.valid := Mux(rQueueEmpty, false.B, master_ports(rQueue.io.deq.bits).R.valid)

    rQueue.io.deq.ready := r_buf.io.in.valid && r_buf.io.in.ready
  }

}

/** Thin test wrapper module for formal verification. This module instantiates the interconnect DUT and connects it to
  * formal master and slave devices. The formal master constrains the slave port of the interconnect to only generate
  * legal transactions, while each master port is connected to a formal slave module with liveness and safety
  * properties.
  *
  * @param addr_width
  *   address bus width
  * @param data_width
  *   data bus width
  * @param mem_ranges
  *   Seq of memory ranges corresponding to memory ranges of connected slaves.
  */
class AXI4LMMDemuxTestWrapper(addr_width: Int, data_width: Int, mem_ranges: Seq[MemoryRange]) extends RawModule {
  val clk_rst = IO(new AXI4LClkRst)

  // Create the memory_map data struct you would have on normal Chisel generators (using Formal modules)
  val formal_master = Module(new AXI4LFormalMaster(addr_width, data_width))
  val memory_map    = mem_ranges.map(x => x -> Module(new AXI4LFormalSlave(addr_width, data_width))).toMap

  // Instantiate interconnect DUT
  val axi4l_mm_demux = Module(new AXI4LMMDemux(addr_width, data_width, mem_ranges))

  axi4l_mm_demux.clk_rst <> clk_rst
  // Connect formal master and slaves to DUT
  formal_master.clk_rst <> clk_rst
  formal_master.master_port <> axi4l_mm_demux.slave_port

  for (((range, slave), i) <- memory_map.zipWithIndex) {

    slave.clk_rst <> clk_rst
    slave.slave_port <> axi4l_mm_demux.master_ports(i)
  }

}

object AXI4LMMDemuxTestWrapper extends App {

  val mem_ranges = Seq(
    MemoryRange(begin = "h0000_1000", end = "h0000_1FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_2000", end = "h0000_2FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_3000", end = "h0000_3FFF", mode = MemoryMode.RW)
  )

  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LMMDemuxTestWrapper(32, 32, mem_ranges)))
  (new ChiselStage).execute(args, annos)
}
