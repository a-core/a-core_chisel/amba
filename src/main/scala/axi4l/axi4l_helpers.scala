// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import amba.common.SkidBuffer
import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** Convert between AXI4-Lite and simple SRAM signaling.
*/
class AXI4LtoSRAM(addr_width: Int, data_width: Int) 
  extends RawModule with AXI4LSlave{
  // AXI4-Lite signaling
  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))
  // SRAM signaling
  val io = IO(SRAMIf(addr_width, data_width))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {

    val ar_buf = Module(new SkidBuffer(new AR(addr_width)))
    val r_buf = Module(new SkidBuffer(new R(data_width)))
    val aw_buf = Module(new SkidBuffer(new AW(data_width)))
    val w_buf = Module(new SkidBuffer(new W(data_width)))
    val b_buf = Module(new SkidBuffer(new B, cut = true))

    val readOk = io.read.req && io.read.gnt
    val writeOk = io.write.req && io.write.gnt

    ar_buf.io.in.bits   := slave_port.AR.bits
    ar_buf.io.in.valid  := slave_port.AR.valid
    ar_buf.io.out.ready := io.read.gnt

    r_buf.io.in.bits.DATA := io.read.data.bits
    r_buf.io.in.bits.RESP := Mux(io.read.fault, Response.SLVERR, Response.OKAY)
    r_buf.io.in.valid     := io.read.data.valid
    r_buf.io.out.ready    := slave_port.R.ready

    aw_buf.io.in.bits   := slave_port.AW.bits
    aw_buf.io.in.valid  := slave_port.AW.valid
    aw_buf.io.out.ready := io.write.gnt

    w_buf.io.in.bits   := slave_port.W.bits
    w_buf.io.in.valid  := slave_port.W.valid
    w_buf.io.out.ready := io.write.gnt

    b_buf.io.in.bits.RESP := Mux(io.write.fault, Response.SLVERR, Response.OKAY)
    b_buf.io.in.valid     := writeOk
    b_buf.io.out.ready    := slave_port.B.ready

    io.read.req   := ar_buf.io.out.valid && r_buf.io.in.ready && clk_rst.ARESETn
    io.read.addr  := ar_buf.io.out.bits.ADDR

    slave_port.AR.ready := ar_buf.io.in.ready
    slave_port.R.valid  := r_buf.io.out.valid
    slave_port.R.bits   := r_buf.io.out.bits

    io.write.req  := aw_buf.io.out.valid && w_buf.io.out.valid && b_buf.io.in.ready && clk_rst.ARESETn
    io.write.addr := aw_buf.io.out.bits.ADDR
    io.write.data := w_buf.io.out.bits.DATA
    io.write.mask := w_buf.io.out.bits.STRB

    slave_port.AW.ready := aw_buf.io.in.ready
    slave_port.W.ready  := w_buf.io.in.ready
    slave_port.B.bits   := b_buf.io.out.bits
    slave_port.B.valid  := b_buf.io.out.valid


    // We could have assertions like these, but not sure yet how to integrate as part of our flow
    //val bbufAssErr = RegNext(b_buf.io.in.valid, init = false.B) && !RegNext(b_buf.io.in.ready, init = false.B) && !b_buf.io.in.valid
    //when (bbufAssErr) {
    //  printf("\n\nHW bug: Valid deasserted before ready!\n\n")
    //}
    //assert(bbufAssErr, "B valid shouldn't go low before ready!")
  }
}

class AXI4LtoSRAMFormalProperties(
  addr_width: Int,
  data_width: Int,
) extends BlackBox(Map("ADDR_WIDTH" -> addr_width, "DATA_WIDTH" -> data_width)) with HasBlackBoxResource {

  val io = IO(new Bundle {
    val axi = new VerilogAXI4LIO(addr_width, data_width)
    val sram = Flipped(SRAMIf(addr_width, data_width))
    val clk_rst = new AXI4LClkRst
  })
  addResource("/AXI4LtoSRAMFormalProperties.v")
}

class AXI4LtoSRAMFormalTest extends Module {
  val addr_width = 32
  val data_width = 32
  val dut = Module(new AXI4LtoSRAM(addr_width, data_width))
  val formal = Module(new AXI4LtoSRAMFormalProperties(addr_width, data_width))
  dut.io <> formal.io.sram
  formal.io.axi.viewAs[AXI4LIO] <> dut.slave_port
  formal.io.clk_rst.ACLK := clock
  formal.io.clk_rst.ARESETn := ~reset.asBool
  dut.clk_rst.ACLK := clock
  dut.clk_rst.ARESETn := ~reset.asBool
}

object AXI4LtoSRAMFormalTest extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LtoSRAMFormalTest))
  (new ChiselStage).execute(args, annos)
}