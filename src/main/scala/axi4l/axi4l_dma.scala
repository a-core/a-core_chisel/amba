// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import amba.common._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

object DMAEndCondition extends ChiselEnum {
  // Loop both addresses until DMA is reset
  val LOOP = Value(0.U)
  // When read address reaches its end address
  val READ = Value(1.U)
  // When write address reaches its end address
  val WRITE = Value(2.U)
  // Whichever reaches its end address first
  val EITHER = Value(3.U)
}

case class DMAControlBundle(addr_width: Int, increment_depth: Int) extends Bundle {
  /* Start address for a DMA transaction */
  val start_addr = UInt(addr_width.W)
  /* End address for a DMA transaction */
  val end_addr = UInt(addr_width.W)
  /* Increment. Next address is `prev_addr + increment.U` */
  val increment = UInt(increment_depth.W)
}

case class DMAControlInterface(n_channels: Int, addr_width: Int, increment_depth: Int) extends Bundle {

  val channel = Vec(
    n_channels,
    new Bundle {
      /* Trigger from software to start DMA transaction */
      val start_soft = Input(Bool())
      /* External trigger to start DMA transaction */
      val start_trig = Input(Bool())
      /* Read related control signals */
      val read = Input(DMAControlBundle(addr_width, increment_depth))
      /* Write related control signals */
      val write = Input(DMAControlBundle(addr_width, increment_depth))
      /* Condition for ending the DMA transaction */
      val end_condition = Input(DMAEndCondition())
      /* Clock cycle delay between read requests. Set 1 for full throughput. */
      val read_delay = Input(UInt(8.W))
      /* Clock cycle delay between write requests Set 1 for full throughput. */
      val write_delay = Input(UInt(8.W))
      /* Transaction done (pulse) */
      val done_trig = Output(Bool())
      /* Transaction done (held high until next transaction) */
      val done_soft = Output(Bool())
    }
  )

}

object DMAState extends ChiselEnum {
  val IDLE, TRANSFER = Value
}

/** DMA (Direct Memory Access) module. Allows automating data transfers from one memory address range to another.
  *
  * @param addr_width
  * @param data_width
  * @param fifo_depth Depth of the internal FIFO.
  * @param increment_depth How much address can be incremented. Max increment `2^increment_depth`.
  */
class DMA(addr_width: Int, data_width: Int, fifo_depth: Int, increment_depth: Int) extends Module {

  val io = IO(new Bundle {
    val mem     = SRAMIf(addr_width, data_width)
    val control = DMAControlInterface(1, addr_width, increment_depth)
  })

  // Internal FIFO - read data goes first through the FIFO before being written
  // Separately store the data and grant
  val dataFifo = Module(new Queue(UInt(data_width.W), entries = fifo_depth, pipe = true, hasFlush = true))
  val gntFifo  = Module(new Queue(Bool(), entries = fifo_depth, pipe = true, hasFlush = true))

  val state             = RegInit(DMAState.IDLE)
  val readAddr          = RegInit(0.U(addr_width.W))
  val writeAddr         = RegInit(0.U(addr_width.W))
  val doneReg           = RegInit(false.B)
  val doneStatusReg     = RegInit(false.B)
  val readDelayCounter  = Reg(UInt(8.W))
  val writeDelayCounter = Reg(UInt(8.W))

  val start = io.control.channel(0).start_soft || io.control.channel(0).start_trig

  io.control.channel(0).done_trig := false.B
  io.mem.write.req       := false.B
  io.mem.read.req        := false.B

  io.mem.write.addr := writeAddr
  io.mem.read.addr  := readAddr

  dataFifo.io.enq.bits  := io.mem.read.data.bits
  dataFifo.io.enq.valid := io.mem.read.data.valid

  gntFifo.io.enq.bits  := io.mem.read.gnt
  gntFifo.io.enq.valid := io.mem.read.gnt

  io.mem.write.data      := dataFifo.io.deq.bits
  io.mem.write.mask      := "hF".U
  dataFifo.io.deq.ready  := io.mem.write.gnt
  gntFifo.io.deq.ready   := io.mem.write.gnt

  val readEnd  = readAddr === io.control.channel(0).read.end_addr && (io.control.channel(0).read.increment =/= 0.U)
  val writeEnd = writeAddr === io.control.channel(0).write.end_addr && (io.control.channel(0).write.increment =/= 0.U)

  dataFifo.io.flush.get := false.B
  gntFifo.io.flush.get  := false.B

  when(io.mem.read.gnt) {
    readAddr := readAddr + io.control.channel(0).read.increment
  }

  when(io.mem.write.gnt) {
    writeAddr := writeAddr + io.control.channel(0).write.increment
  }

  io.control.channel(0).done_soft := doneStatusReg

  val readTime  = readDelayCounter  === io.control.channel(0).read_delay
  val writeTime = writeDelayCounter === io.control.channel(0).write_delay

  when(state === DMAState.IDLE) {
    when(start) {
      state                 := DMAState.TRANSFER
      readAddr              := io.control.channel(0).read.start_addr
      writeAddr             := io.control.channel(0).write.start_addr
      dataFifo.io.flush.get := true.B
      gntFifo.io.flush.get  := true.B
      doneStatusReg         := false.B
      doneReg               := false.B
      readDelayCounter      := io.control.channel(0).read_delay
      writeDelayCounter     := io.control.channel(0).write_delay
    }
  }.elsewhen(state === DMAState.TRANSFER) {

    when (readTime && io.mem.read.gnt) {
      readDelayCounter := 1.U
    } .elsewhen (!readTime) {
      readDelayCounter := readDelayCounter + 1.U
    } 

    when (writeTime && io.mem.write.gnt) {
      writeDelayCounter := 1.U
    } .elsewhen (!writeTime) {
      writeDelayCounter := writeDelayCounter + 1.U
    } 

    // Generate read request if gntFifo is not full
    // Don't generate request if doneReg is high
    io.mem.read.req  := gntFifo.io.enq.ready && !doneReg && readTime

    // Generate write request if dataFifo is not empty
    io.mem.write.req := dataFifo.io.deq.valid && writeTime

    // When read address reaches its end address
    // and grant has been given
    when(readEnd && io.mem.read.gnt) {
      // Wrap read address to the start address
      readAddr := io.control.channel(0).read.start_addr
      // If end condition is READ or EITHER, remain in TRANSFER until FIFO is empty
      when (io.control.channel(0).end_condition.isOneOf(Seq(DMAEndCondition.READ, DMAEndCondition.EITHER))) {
        doneReg := true.B
      }
    }

    // When write address reaches its end address
    // and grant has been given
    when(writeEnd && io.mem.write.gnt) {
      // Wrap write address to the start address
      writeAddr := io.control.channel(0).write.start_addr
      // If end condition is WRITE or EITHER, move to IDLE state
      when (io.control.channel(0).end_condition.isOneOf(Seq(DMAEndCondition.WRITE, DMAEndCondition.EITHER))) {
        state                           := DMAState.IDLE
        doneReg                         := true.B
        io.control.channel(0).done_trig := true.B
        doneStatusReg                   := true.B
      }
    }

    // When doneReg is high and FIFO is empty
    // move to IDLE state
    when (doneReg && !gntFifo.io.deq.valid) {
      state                           := DMAState.IDLE
      io.control.channel(0).done_trig := true.B
      doneStatusReg                   := true.B
    }


  }
}

/** AXI4-Lite wrapper for DMA. Contains one master port (`io.axi4l`) and one slave port (`slave_port`). Master port is
  * used for DMA transactions. Slave port is used for DMA controls.
  * 
  * Usage:
    - Define start and end addresses for read and write targets. Define the increment by which the address is
    increased after each transaction. Increment can be a positive value or zero.
    - Start DMA transaction by writing to `control` register, `software_start` bit. Alternatively, an external
      trigger can start the transaction.
    - If you want to perform the transaction (from start to end) only once, clear the `software_start` bit
      immediately after writing it. Otherwise, the DMA will loop.
    - Poll `status` register's `done` bit, it will go high when transactions are done, assuming the DMA is not looping.
      Alternatively, there is a pulse signal that indicates end of transaction, which can be used to interrupt the processor.

  * Note: fifo_depth should be at least 2 to allow full throughput for slaves with one clock cycle read and writes. Fifo depth of 1
  * will result in 50% of max throughput.
  * If there is a longer delay than 1 clock cycle in memory accesses, fifo_depth should be increased further.
  *
  * @param addr_width Address width
  * @param data_width Data width
  * @param fifo_depth How many levels does the internal FIFO include.
  * @param increment_depth How much address can be incremented. Max increment `2^increment_depth`.
  */
class AXI4LDMA(val addr_width: Int, val data_width: Int, fifo_depth: Int, increment_depth: Int = 4)
  extends RawModule 
  with AXI4LSlaveLogic 
  with AXI4LSlave {

  val io = IO(new Bundle {
    val axi4l   = Flipped(new AXI4LIO(addr_width, data_width))
    val start_trigger = Input(Bool())
    val done_trigger = Output(Bool())
  })

  val axi4l = Module(new AXI4LMasterInterface(addr_width, data_width))

  val delay_reg      = Wire(UInt(32.W))
  val control_reg    = WireDefault(0.U(data_width.W))
  val dma_soft_reset = control_reg(0)

  val dma = withClockAndReset(clk_rst.ACLK, (~clk_rst.ARESETn.asBool || dma_soft_reset)) {
    Module(new DMA(addr_width, data_width, fifo_depth, increment_depth))
  }

  val status_reg = Cat(0.U((data_width - 1).W), dma.io.control.channel(0).done_soft)

  val memory_map = Map[Data, MemoryRange](
    dma.io.control.channel(0).read.start_addr -> MemoryRange(
      begin = 0,
      end = 3,
      mode = MemoryMode.RW,
      name = "read_start_addr",
    ),
    dma.io.control.channel(0).read.end_addr -> MemoryRange(
      begin = 4,
      end = 7,
      mode = MemoryMode.RW,
      name = "read_end_addr"
    ),
    dma.io.control.channel(0).write.start_addr -> MemoryRange(
      begin = 8,
      end = 11,
      mode = MemoryMode.RW,
      name = "write_start_addr"
    ),
    dma.io.control.channel(0).write.end_addr -> MemoryRange(
      begin = 12,
      end = 15,
      mode = MemoryMode.RW,
      name = "write_end_addr"
    ),
    dma.io.control.channel(0).read.increment -> MemoryRange(
      begin = 16,
      end = 19,
      mode = MemoryMode.RW,
      name = "read_increment"
    ),
    dma.io.control.channel(0).write.increment -> MemoryRange(
      begin = 20,
      end = 23,
      mode = MemoryMode.RW,
      name = "write_increment"
    ),
    control_reg -> MemoryRange(
      begin = 24,
      end = 27,
      mode = MemoryMode.RW,
      name = "control",
      bitfields = List(
        BitField(begin = 0, end = 0, init = 0, autoClear = true, name = "soft_reset",
                 description = "Reset the DMA"),
        BitField(begin = 8, end = 8, init = 0, autoClear = true, name = "software_start",
                 description = "Start the DMA transaction"),
        BitField(begin = 16, end = 17, init = 3, autoClear = false, name = "end_condition",
                 description = """|End condition:
                                  |00 -> LOOP - DMA loops read and write addresses until it's reset
                                  |01 -> READ - Transfer until read address reaches end address
                                  |10 -> WRITE - Transfer until write address reaches end address
                                  |11 -> EITHER - Transfer until either read or write reaches its end address""".stripMargin)
      )
    ),
    status_reg -> MemoryRange(begin = 28, end = 31, mode = MemoryMode.R, name = "status", bitfields = List(
      BitField(begin = 0, end = 0, name = "done",
               description = """|Done signal indicating the DMA transfer has finished succesfully.
                                |If end condition is loop (00), done signal will not be asserted.""".stripMargin)
    )),
    delay_reg -> MemoryRange(begin = 32, end = 35, mode = MemoryMode.RW, name = "delay", bitfields = List(
      BitField(begin = 0, end = 7, name = "read_delay", init = 1, description = "Clock cycle delay between reads. Set 1 for highest throughput."),
      BitField(begin = 8, end = 15, name = "write_delay", init = 1, description = "Clock cycle delay between writes. Set 1 for highest throughput.")
    ))
  )

  buildAxi4LiteSlaveLogic()

  axi4l.io.clk_rst <> clk_rst
  axi4l.io.axi4l <> io.axi4l
  axi4l.io.core <> dma.io.mem
  dma.io.control.channel(0).start_trig := io.start_trigger
  dma.io.control.channel(0).start_soft := control_reg(8)
  dma.io.control.channel(0).end_condition := DMAEndCondition(control_reg(17,16))
  dma.io.control.channel(0).read_delay := delay_reg(7,0)
  dma.io.control.channel(0).write_delay := delay_reg(15,8)
  io.done_trigger := dma.io.control.channel(0).done_trig
}

/** Testbench with DMA, demux interconnect, and three slave ports */
class AXI4LDMATestWrapper(addr_width: Int, data_width: Int) extends Module {

  val io = IO(new Bundle {
    val axi_slave    = Vec(2, new VerilogAXI4LIO(addr_width, data_width))
    val control_port = Flipped(new VerilogAXI4LIO(addr_width, data_width))
    val start_trigger      = Input(Bool())
    val done_trigger      = Output(Bool())
  })

  val mem_ranges = Seq(
    MemoryRange(begin = "h0000_1000", end = "h0000_1FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_2000", end = "h0000_2FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_3000", end = "h0000_3FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_4000", end = "h0000_4FFF", mode = MemoryMode.RW)
  )

  val dut  = Module(new AXI4LDMA(addr_width, data_width, 6, 14))
  val ic   = Module(new AXI4LMMDemux(addr_width, data_width, mem_ranges))
  val mem1 = Module(new AXI4LSRAM(addr_width, data_width, mem_depth = 8))
  val mem2 = Module(new AXI4LSRAM(addr_width, data_width, mem_depth = 8))

  // Connect first two ports to Python models defined in cocotb testbench
  for (i <- 0 until 2) {
    io.axi_slave(i).viewAs[AXI4LIO] <> ic.master_ports(i)
    // cocotb-axi does not accept 000 PROT code even though it is fully legit
    io.axi_slave(i).arprot := "b010".U
    io.axi_slave(i).awprot := "b010".U
  }

  // Connect latter two ports to AXI4LSRAM instances
  mem1.slave_port <> ic.master_ports(2)
  mem2.slave_port <> ic.master_ports(3)

  dut.io.start_trigger := io.start_trigger
  io.done_trigger      := dut.io.done_trigger
  // Connect clocks and resets
  mem1.clk_rst.ACLK    := clock
  mem2.clk_rst.ACLK    := clock
  mem1.clk_rst.ARESETn := ~reset.asBool
  mem2.clk_rst.ARESETn := ~reset.asBool
  ic.clk_rst.ACLK      := clock
  ic.clk_rst.ARESETn   := ~reset.asBool
  ic.slave_port <> dut.io.axi4l
  io.control_port.viewAs[AXI4LIO] <> dut.slave_port
  dut.clk_rst.ACLK    := clock
  dut.clk_rst.ARESETn := ~reset.asBool
}

object AXI4LDMATestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LDMATestWrapper(32, 32)))
  (new ChiselStage).execute(args, annos)
}
