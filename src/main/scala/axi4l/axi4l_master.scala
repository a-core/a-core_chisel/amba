// SPDX-License-Identifier: Apache-2.0

// Initially written by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-12-18
package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.axi4l._
import scala.math.log

case class AXI4LMasterBundle(addr_width: Int, data_width: Int, with_wresp: Boolean = false) extends Bundle {
  val clk_rst = new AXI4LClkRst
  val axi4l   = Flipped(new AXI4LIO(addr_width, data_width))
  val core    = Flipped(SRAMIf(addr_width, data_width, with_wresp))
}

/** SRAM-like memory interface */
case class SRAMIf(addr_width: Int, data_width: Int, with_wresp: Boolean = false) extends Bundle {
  val read  = new Bundle {
    /** Request */
    val req   = Output(Bool())
    /** Address */
    val addr  = Output(UInt(addr_width.W))
    /** Data */
    val data  = Flipped(Valid(UInt(data_width.W)))
    /** Request granted */
    val gnt   = Input(Bool())
    /** Request ended in a fault */
    val fault = Input(Bool())
  }
  val write = new Bundle {
    /** Request */
    val req   = Output(Bool())
    /** Address */
    val addr  = Output(UInt(addr_width.W))
    /** Data */
    val data  = Output(UInt(data_width.W))
    /** Write mask. One bit masks 8 bits. Use "hF".U for 32-bits. */
    val mask  = Output(UInt((data_width / 8).W))
    /** Request granted */
    val gnt   = Input(Bool())
    /** Request ended in a fault. Valid when resp == true */
    val fault = Input(Bool())
    val resp  = if (with_wresp) Some(Input(Bool())) else None
  }
}

/**
  * A simple AXI4L Master interface. Converts SRAM signals to AXI4L signals.
  * No masking functionality.
  *
  * @param addr_width
  * @param data_width
  */
class AXI4LMasterInterface(addr_width: Int, data_width: Int, with_wresp: Boolean = false) extends RawModule {

  val io = IO(new AXI4LMasterBundle(addr_width, data_width, with_wresp))
  val resetDone = io.clk_rst.ARESETn && withClock(io.clk_rst.ACLK) { RegNext(io.clk_rst.ARESETn) }

  withClockAndReset(io.clk_rst.ACLK, ~resetDone) {

    // FIFOs used to maintain full throughput

    // Read FIFO
    val rQueue      = Module(new Queue(Bool(), 4))
    val rQueueFull  = !rQueue.io.enq.ready
    val rQueueEmpty = !rQueue.io.deq.valid
    rQueue.io.enq.bits  := io.core.read.req
    rQueue.io.enq.valid := io.core.read.gnt
    rQueue.io.deq.ready := io.axi4l.R.fire

    // Write FIFO
    val wQueue      = Module(new Queue(Bool(), 4))
    val wQueueFull  = !wQueue.io.enq.ready
    val wQueueEmpty = !wQueue.io.deq.valid
    wQueue.io.enq.bits  := io.core.write.req
    wQueue.io.enq.valid := io.core.write.gnt
    wQueue.io.deq.ready := io.axi4l.B.fire

    io.axi4l.AW.valid     := false.B
    io.axi4l.AW.bits.ADDR := io.core.write.addr
    io.axi4l.AW.bits.PROT := 0.U
    io.axi4l.W.bits.DATA  := io.core.write.data
    io.axi4l.W.bits.STRB  := io.core.write.mask
    io.axi4l.W.valid      := false.B
    io.axi4l.AR.bits.ADDR := io.core.read.addr
    io.axi4l.AR.bits.PROT := 0.U
    io.axi4l.AR.valid     := false.B
    io.axi4l.B.ready      := !wQueueEmpty && wQueue.io.deq.bits
    io.axi4l.R.ready      := !rQueueEmpty && rQueue.io.deq.bits

    io.core.read.gnt  := false.B
    io.core.write.gnt := false.B

    io.core.read.data.bits  := io.axi4l.R.bits.DATA
    io.core.read.data.valid := io.axi4l.R.valid
    io.core.read.fault      := Mux(rQueue.io.deq.bits && io.axi4l.R.valid, io.axi4l.R.bits.RESP =/= Response.OKAY, false.B)

    io.core.write.fault    := Mux(wQueue.io.deq.bits && io.axi4l.B.valid, io.axi4l.B.bits.RESP =/= Response.OKAY, false.B)
    if (with_wresp) {
      io.core.write.resp.get := io.axi4l.B.fire
    }

    // Read request
    when (io.core.read.req && !rQueueFull && resetDone) {
      io.axi4l.AR.valid := true.B
      io.core.read.gnt := io.axi4l.AR.ready
    }

    val awSent = RegInit(false.B)
    val wSent  = RegInit(false.B)

    // Write request
    when (io.core.write.req && !wQueueFull && resetDone) {
      when (!awSent && !wSent) {
        io.axi4l.AW.valid := true.B
        io.axi4l.W.valid  := true.B
        io.core.write.gnt := io.axi4l.AW.ready && io.axi4l.W.ready
        when (!(io.axi4l.AW.ready && io.axi4l.W.ready)) {
          awSent := Mux(awSent, awSent, io.axi4l.AW.ready)
          wSent  := Mux(wSent, wSent, io.axi4l.W.ready)
        }
      } .elsewhen (awSent && !wSent) {
        io.axi4l.W.valid := true.B
        when (io.axi4l.W.ready) {
          awSent            := false.B
          io.core.write.gnt := true.B
        }
      } .elsewhen (!awSent && wSent) {
        io.axi4l.AW.valid := true.B
        when (io.axi4l.AW.ready) {
          wSent             := false.B
          io.core.write.gnt := true.B
        }
      } .otherwise {
        awSent := false.B
        wSent  := false.B
      }
    }

  }
}
