// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import amba.common.SkidBuffer
import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** 
 *  Insert a cut to combinational paths in an AXI4-Lite bus
*/
class AXI4LCut(addr_width: Int, data_width: Int) 
  extends RawModule with AXI4LSlave{
  // AXI4-Lite signaling
  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))
  val master_port = IO(Flipped(new AXI4LIO(addr_width=addr_width, data_width=data_width)))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {
    val bufAR = Module(new SkidBuffer(new AR(addr_width), cut = true))
    val bufR  = Module(new SkidBuffer(new R(data_width), cut = true))
    val bufAW = Module(new SkidBuffer(new AW(addr_width), cut = true))
    val bufW  = Module(new SkidBuffer(new W(data_width), cut = true))
    val bufB  = Module(new SkidBuffer(new B, cut = true))

    bufAR.io.in  <> slave_port.AR
    bufAR.io.out <> master_port.AR
    bufR.io.in   <> master_port.R
    bufR.io.out  <> slave_port.R
    bufAW.io.in  <> slave_port.AW
    bufAW.io.out <> master_port.AW
    bufW.io.in   <> slave_port.W
    bufW.io.out  <> master_port.W
    bufB.io.in   <> master_port.B
    bufB.io.out  <> slave_port.B
  }
}

