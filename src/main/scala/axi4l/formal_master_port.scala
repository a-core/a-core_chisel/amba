// SPDX-License-Identifier: Apache-2.0

package amba.axi4l.formal

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.axi4l._
import amba.common._

/** Mock AXI4-Lite master device containing formal properties */
class AXI4LFormalMaster(
    addr_width: Int = 32,
    data_width: Int = 32
) extends RawModule {
  val clk_rst     = IO(new AXI4LClkRst)
  val master_port = IO(Flipped(new AXI4LIO(addr_width = 32, data_width = 32)))
  val properties  = Module(new AXI4LMasterFormalProperties(addr_width, data_width))
  dontTouch(master_port)

  // AXI Clock and Reset
  properties.io.ACLK    := clk_rst.ACLK
  properties.io.ARESETn := clk_rst.ARESETn
  // Read Address Channel (done)
  properties.io.ARREADY    := master_port.AR.ready
  master_port.AR.valid     := properties.io.ARVALID
  master_port.AR.bits.ADDR := properties.io.ARADDR
  master_port.AR.bits.PROT := properties.io.ARPROT
  // Read Data Channel (done)
  master_port.R.ready  := properties.io.RREADY
  properties.io.RVALID := master_port.R.valid
  properties.io.RDATA  := master_port.R.bits.DATA
  properties.io.RRESP  := master_port.R.bits.RESP.asUInt
  // Write Address Channel
  properties.io.AWREADY    := master_port.AW.ready
  master_port.AW.valid     := properties.io.AWVALID
  master_port.AW.bits.ADDR := properties.io.AWADDR
  master_port.AW.bits.PROT := properties.io.AWPROT
  // Write Data Channel
  properties.io.WREADY    := master_port.W.ready
  master_port.W.valid     := properties.io.WVALID
  master_port.W.bits.DATA := properties.io.WDATA
  master_port.W.bits.STRB := properties.io.WSTRB
  // Write Response Channel
  master_port.B.ready  := properties.io.BREADY
  properties.io.BVALID := master_port.B.valid
  properties.io.BRESP  := master_port.B.bits.RESP.asUInt
}

/** Generate Verilog */
object AXI4LFormalMaster extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LFormalMaster(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}

/** BlackBox of the AXI4-Lite master port formal assertions written in verilog */
class AXI4LMasterFormalProperties(
    addr_width: Int,
    data_width: Int
) extends BlackBox(Map("ADDR_WIDTH" -> addr_width, "DATA_WIDTH" -> data_width)) with HasBlackBoxResource {

  val io = IO(new Bundle {
    // Clock and Reset
    val ACLK    = Input(Clock())
    val ARESETn = Input(Bool())
    // Read Address Channel
    val ARREADY = Input(Bool())
    val ARVALID = Output(Bool())
    val ARADDR  = Output(UInt(addr_width.W))
    val ARPROT  = Output(UInt(3.W))
    // Read Data Channel
    val RREADY = Output(Bool())
    val RVALID = Input(Bool())
    val RDATA  = Input(UInt(data_width.W))
    val RRESP  = Input(UInt(2.W))
    // Write Address Channel
    val AWREADY = Input(Bool())
    val AWVALID = Output(Bool())
    val AWADDR  = Output(UInt(addr_width.W))
    val AWPROT  = Output(UInt(3.W))
    // Write Data Channel
    val WREADY = Input(Bool())
    val WVALID = Output(Bool())
    val WDATA  = Output(UInt(data_width.W))
    val WSTRB  = Output(UInt((data_width / 8).W))
    // Write Response Channel
    val BREADY = Output(Bool())
    val BVALID = Input(Bool())
    val BRESP  = Input(UInt(2.W))
  })

  addResource("/AXI4LMasterFormalProperties.v")
}
