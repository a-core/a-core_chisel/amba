// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.common._
import amba.axi4l.formal._

/**
  * Wrapper module for two different arbiter types with the same IO.
  * Round-robin arbitration provides access equally to all requesters. In case of simultaneous access,
  * it gives prioirty to the lowest index, but on the next turn, it handles to the next index.
  * Fixed arbitration provides priority to lowest index always.
  *
  * @param gen
  *   Data type
  * @param n
  *   Number of requesters
  * @param arb_type
  *   Arbitration type (round-robin, fixed)
  */
class SupportedArbiters[T <: Data](gen: T, n: Int, arb_type: String) extends Module {
  val io = IO(new ArbiterIO(gen, n))

  /**
    * RRArbiter which provides an initial value for an internal register.
    * Otherwise, undefined values would propagate in the simulator.
    *
    * @param gen
    * @param n
    */
  class CustomRRArbiter[T <: Data](override val gen: T, override val n: Int) extends RRArbiter[T](gen, n) {
    override lazy val lastGrant = RegEnable(io.chosen, 0.U, io.out.fire)
  }

  arb_type match {
    case "round-robin" => {
      val arb = Module(new CustomRRArbiter(gen, n))
      arb.io <> io
    }
    case "fixed" => {
      val arb = Module(new Arbiter(gen, n))
      arb.io <> io
    }
    case _ => throw new Exception(s"Wrong type of arbitration selected: $arb_type")
  }
}

/** Arbiter for connecting n masters into 1 slave. Configurable arbitration type.
  *
  * @param n_masters
  *   Number of masters
  * @param addr_width
  *   address width
  * @param data_width
  *   data width
  * @param arb_type
  *   Arbitration type. "round-robin" or "fixed"
  * @param queue_depth
  *   How deep the internal FIFOs are
  */
class AXI4LMasterArbiter(n_masters: Int, addr_width: Int, data_width: Int, arb_type: String, queue_depth: Int = 2) extends RawModule {

  val clk_rst     = IO(new AXI4LClkRst)
  val slave_ports = IO(Vec(n_masters, new AXI4LIO(addr_width, data_width)))
  val master_port = IO(Flipped(new AXI4LIO(addr_width, data_width)))

  withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {

    /* -------------------- Skid buffers  ----------------- */
    // These modules let data through by default, but if there
    // is a case where in.valid == true and out.ready == false,
    // the data is stored into a buffer register.
    // This allows a full-throughput interconnect while still having
    // decoupled ready and valid signals (i.e. not combinationally dependent)

    val ar_buf = Module(new SkidBuffer(new AW(addr_width)))
    val r_buf = Module(new SkidBuffer(new R(data_width)))
    val aw_buf = Module(new SkidBuffer(new AR(addr_width)))
    val w_buf = Module(new SkidBuffer(new W(data_width)))
    val b_buf = Module(new SkidBuffer(new B))

    /* --------------------  Arbiters  -------------------- */
    // See SupportedArbiters class for further info

    // AW channel arbiter
    val awArbiter = Module(new SupportedArbiters(new AW(addr_width), n_masters, arb_type))
    awArbiter.io.out.ready := false.B

    // AR channel arbiter
    val arArbiter = Module(new SupportedArbiters(new AR(addr_width), n_masters, arb_type))
    arArbiter.io.out.ready := false.B

    /* --------------------    FIFOs   -------------------- */
    // FIFOs store the destination port. Thus, AW channel can be freed once a valid
    // address has been sent, since the target slave is stored in wQueue and bQueue.
    // Same for AR channel.
    // ``flow = true`` means that enq.valid traverses to deq.valid combinationally.
    // It is enabled for wQueue so that AWVALID and WVALID assert simultaneously.
    // This reduces latency by 1 clock cycle.
    // For rQueue and bQueue it is disabled as it shortens combinational path
    // and latency is unaffected.

    // Write data FIFO
    val wQueue = Module(new Queue(chiselTypeOf(awArbiter.io.chosen), queue_depth, flow = true))
    val wQueueFull       = !wQueue.io.enq.ready
    val wQueueEmpty      = !wQueue.io.deq.valid
    wQueue.io.enq.bits  := awArbiter.io.chosen
    wQueue.io.enq.valid := false.B
    wQueue.io.deq.ready := false.B

    // Write response data FIFO
    val bQueue = Module(new Queue(chiselTypeOf(awArbiter.io.chosen), queue_depth, flow = false))
    val bQueueFull       = !bQueue.io.enq.ready
    val bQueueEmpty      = !bQueue.io.deq.valid
    bQueue.io.enq.bits  := wQueue.io.deq.bits
    bQueue.io.enq.valid := wQueue.io.deq.valid && wQueue.io.deq.ready
    bQueue.io.deq.ready := false.B

    // Read data FIFO
    val rQueue = Module(new Queue(chiselTypeOf(arArbiter.io.chosen), queue_depth, flow = false))
    val rQueueFull       = !rQueue.io.enq.ready
    val rQueueEmpty      = !rQueue.io.deq.valid
    rQueue.io.enq.bits  := arArbiter.io.chosen
    rQueue.io.enq.valid := false.B
    rQueue.io.deq.ready := false.B

    /* -------------------- AW channel -------------------- */


    for (i <- 0 until n_masters) {
      awArbiter.io.in(i) <> slave_ports(i).AW
    }

    master_port.AW.bits  := aw_buf.io.out.bits
    master_port.AW.valid := aw_buf.io.out.valid
    aw_buf.io.in.bits   := awArbiter.io.out.bits
    aw_buf.io.in.valid  := false.B
    aw_buf.io.out.ready := master_port.AW.ready

    val lockAwValid = RegInit(false.B)

    when (lockAwValid) {
      aw_buf.io.in.valid := true.B
      when (aw_buf.io.in.ready) {
        awArbiter.io.out.ready := true.B
        lockAwValid := false.B
      }
    } .elsewhen (!wQueueFull && awArbiter.io.out.valid) {
      aw_buf.io.in.valid  := true.B
      wQueue.io.enq.valid := true.B
      when (aw_buf.io.in.ready) {
        awArbiter.io.out.ready := true.B
      } .otherwise {
        lockAwValid := true.B
      }
    }

    /* --------------------  W channel -------------------- */

    master_port.W.bits  := w_buf.io.out.bits
    master_port.W.valid := w_buf.io.out.valid
    w_buf.io.in.bits := slave_ports(wQueue.io.deq.bits).W.bits
    w_buf.io.out.ready := master_port.W.ready
    w_buf.io.in.valid := Mux(!wQueueEmpty && !bQueueFull, slave_ports(wQueue.io.deq.bits).W.valid, false.B)
    wQueue.io.deq.ready := w_buf.io.in.valid && w_buf.io.in.ready

    for (i <- 0 until n_masters) {
      slave_ports(i).W.ready := w_buf.io.in.ready && !wQueueEmpty && !bQueueFull && (wQueue.io.deq.bits === i.U)
    }

    /* --------------------  B channel -------------------- */

    master_port.B.ready := b_buf.io.in.ready
    b_buf.io.in.bits  := master_port.B.bits
    b_buf.io.in.valid := master_port.B.valid
    b_buf.io.out.ready := !bQueueEmpty && slave_ports(bQueue.io.deq.bits).B.ready
    bQueue.io.deq.ready := b_buf.io.out.ready && b_buf.io.out.valid

    for (i <- 0 until n_masters) {
      slave_ports(i).B.bits  := b_buf.io.out.bits
      slave_ports(i).B.valid := b_buf.io.out.valid && !bQueueEmpty && (bQueue.io.deq.bits === i.U)
    }

    /* -------------------- AR channel -------------------- */

    for (i <- 0 until n_masters) {
      arArbiter.io.in(i) <> slave_ports(i).AR
    }

    master_port.AR.bits := ar_buf.io.out.bits
    master_port.AR.valid := ar_buf.io.out.valid
    ar_buf.io.in.bits := arArbiter.io.out.bits
    ar_buf.io.out.ready := master_port.AR.ready
    ar_buf.io.in.valid := Mux(rQueueFull, false.B, arArbiter.io.out.valid)
    arArbiter.io.out.ready := Mux(rQueueFull, false.B, ar_buf.io.in.ready)
    rQueue.io.enq.valid := ar_buf.io.in.valid && ar_buf.io.in.ready

    /* --------------------  R channel -------------------- */

    r_buf.io.in.bits  := master_port.R.bits
    r_buf.io.in.valid := master_port.R.valid
    r_buf.io.out.ready  := !rQueueEmpty && slave_ports(rQueue.io.deq.bits).R.ready
    master_port.R.ready := r_buf.io.in.ready
    rQueue.io.deq.ready := r_buf.io.out.valid && r_buf.io.out.ready

    for (i <- 0 until n_masters) {
      slave_ports(i).R.bits  := r_buf.io.out.bits
      slave_ports(i).R.valid := r_buf.io.out.valid && !rQueueEmpty && (rQueue.io.deq.bits === i.U)
    }

  }

}

class AXI4LMasterArbiterTestWrapper(n_masters: Int, addr_width: Int, data_width: Int, arb_type: String) extends RawModule {

  val clk_rst = IO(new AXI4LClkRst)

  val formal_slave         = Module(new AXI4LFormalSlave(addr_width, data_width))
  val axi4l_master_arbiter = Module(new AXI4LMasterArbiter(n_masters, addr_width, data_width, arb_type))
  val formal_masters       = Seq.fill(n_masters)(Module(new AXI4LFormalMaster(addr_width, data_width)))
  formal_slave.slave_port <> axi4l_master_arbiter.master_port
  axi4l_master_arbiter.clk_rst <> clk_rst

  for ((master, i) <- formal_masters.zipWithIndex) {
    master.clk_rst <> clk_rst
    master.master_port <> axi4l_master_arbiter.slave_ports(i)
  }

  formal_slave.clk_rst <> clk_rst
}

// Test wrapper with arbiter and demux
class AXI4LArbiterDemuxTestWrapper(n_masters: Int, mem_ranges: Seq[MemoryRange], addr_width: Int, data_width: Int)
    extends RawModule {

  val clk_rst = IO(new AXI4LClkRst)

  val memory_map = mem_ranges.map(x => x -> Module(new AXI4LFormalSlave(addr_width, data_width))).toMap

  val axi4l_master_arbiter = Module(new AXI4LMasterArbiter(n_masters, addr_width, data_width, "round-robin"))
  val axi4l_mm_demux       = Module(new AXI4LMMDemux(addr_width, data_width, mem_ranges))

  val formal_masters = Seq.fill(n_masters)(Module(new AXI4LFormalMaster(addr_width, data_width)))

  axi4l_mm_demux.slave_port <> axi4l_master_arbiter.master_port
  axi4l_master_arbiter.clk_rst <> clk_rst
  axi4l_mm_demux.clk_rst <> clk_rst

  // Connect masters
  for ((master, i) <- formal_masters.zipWithIndex) {
    master.clk_rst <> clk_rst
    master.master_port <> axi4l_master_arbiter.slave_ports(i)
  }

  // Connect slaves
  for (((range, slave), i) <- memory_map.zipWithIndex) {
    slave.clk_rst <> clk_rst
    slave.slave_port <> axi4l_mm_demux.master_ports(i)
  }

}

object AXI4LMasterArbiterTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LMasterArbiterTestWrapper(3, 32, 32, "round-robin")))
  (new ChiselStage).execute(args, annos)
}

object AXI4LArbiterDemuxTestWrapper extends App {

  val mem_ranges = Seq(
    MemoryRange(begin = "h0000_1000", end = "h0000_1FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_2000", end = "h0000_2FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_3000", end = "h0000_3FFF", mode = MemoryMode.RW)
  )

  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LArbiterDemuxTestWrapper(3, mem_ranges, 32, 32)))
  (new ChiselStage).execute(args, annos)
}
