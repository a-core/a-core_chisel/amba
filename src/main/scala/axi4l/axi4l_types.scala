// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

object Response extends ChiselEnum {
  val OKAY   = Value("b00".U(2.W)) // Normal access success.
  val EXOKAY = Value("b01".U(2.W)) // Exclusive access okay.
  val SLVERR = Value("b10".U(2.W)) // Subordinate error.
  val DECERR = Value("b11".U(2.W)) // Decode error.
}

/* Read Address Channel Bundle */
class AR(width: Int) extends Bundle {
  val ADDR = UInt(width.W)
  val PROT = UInt(3.W)
}

/* Read Data Channel Bundle */
class R(width: Int) extends Bundle {
  val DATA = UInt(width.W)
  val RESP = Response()
}

/* Write Address Channel Bundle */
class AW(width: Int) extends Bundle {
  val ADDR = UInt(width.W)
  val PROT = UInt(3.W)
}

/* Write Data Channel Bundle */
class W(width: Int) extends Bundle {
  val DATA = UInt(width.W)
  val STRB = UInt((width / 8).W)
}

/* Write Response Channel Bundle */
class B extends Bundle {
  val RESP = Response()
}

/* AXI4-Lite Clock and Reset port */
class AXI4LClkRst extends Bundle {
  val ACLK    = Input(Clock())
  val ARESETn = Input(Bool())
}

/** AXI4-Lite Slave/Master port signals */
// https://www.chisel-lang.org/chisel3/docs/explanations/interfaces-and-connections.html
// Channel directions are configured for a slave port by default as there are more slaves.
class AXI4LIO(addr_width: Int, data_width: Int) extends Bundle {
  // Read Address Channel (M -> S)
  val AR = Flipped(Irrevocable(new AR(addr_width)))
  // Read Data Channel (M <- S)
  val R = Irrevocable(new R(data_width))
  // Write Address Channel (M -> S)
  val AW = Flipped(Irrevocable(new AW(addr_width)))
  // Write Data Channel (M -> S)
  val W = Flipped(Irrevocable(new W(data_width)))
  // Write Response Channel (M <- S)
  val B = Irrevocable(new B)
}

object AXI4LIO {
  
  implicit val axi4lView = dataview.DataView[VerilogAXI4LIO, AXI4LIO] (
    vab => new AXI4LIO(vab.addr_width, vab.data_width),
    _.araddr  -> _.AR.bits.ADDR,
    _.arprot  -> _.AR.bits.PROT,
    _.arvalid -> _.AR.valid, 
    _.arready -> _.AR.ready,
    _.awaddr  -> _.AW.bits.ADDR,
    _.awprot  -> _.AW.bits.PROT,
    _.awvalid -> _.AW.valid, 
    _.awready -> _.AW.ready,
    _.rdata   -> _.R.bits.DATA,
    _.rresp   -> _.R.bits.RESP,
    _.rvalid  -> _.R.valid, 
    _.rready  -> _.R.ready,
    _.wdata   -> _.W.bits.DATA,
    _.wstrb   -> _.W.bits.STRB,
    _.wvalid  -> _.W.valid, 
    _.wready  -> _.W.ready,
    _.bresp   -> _.B.bits.RESP,
    _.bvalid  -> _.B.valid, 
    _.bready  -> _.B.ready
  )
}

class VerilogAXI4LIO(val addr_width: Int, val data_width: Int) extends Bundle {
  val araddr  = Output(UInt(addr_width.W))
  val arprot  = Output(UInt(3.W))
  val arvalid = Output(Bool())
  val arready = Input(Bool())
  val awaddr  = Output(UInt(addr_width.W))
  val awprot  = Output(UInt(3.W))
  val awvalid = Output(Bool())
  val awready = Input(Bool())
  val rdata   = Input(UInt(data_width.W))
  val rresp   = Input(Response())
  val rvalid  = Input(Bool())
  val rready  = Output(Bool())
  val wdata   = Output(UInt(data_width.W))
  val wstrb   = Output(UInt((data_width / 8).W))
  val wready  = Input(Bool())
  val wvalid  = Output(Bool())
  val bresp   = Input(Response())
  val bvalid  = Input(Bool())
  val bready  = Output(Bool())
}


trait AXI4LSlave {
  val clk_rst: AXI4LClkRst
  val slave_port: AXI4LIO
}

trait AXI4LMaster {
  val clk_rst: AXI4LClkRst
  val master_port: AXI4LIO
}
