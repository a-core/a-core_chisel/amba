// SPDX-License-Identifier: Apache-2.0

// Contributors:
// - Verneri Hirvonen
// - Aleksi Korsman
// - Phuoc Tai Vo (Receiver logic)

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import amba.common._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** Chisel enumeration of RV32I opcodes */
object UartTxState extends ChiselEnum
{
  val IDLE       = Value(0.U)
  val START      = Value(1.U)
  val SHIFT_DATA = Value(2.U)
  val STOP       = Value(3.U) 
}

object UartRxState extends ChiselEnum
{
  val IDLE      = Value(0.U)
  val START_BIT = Value(1.U)
  val DATA_BITS = Value(2.U)
  val STOP_BIT  = Value(3.U)
}

case class UartTxIO() extends Bundle {
  // Transmit channel
  val tx = Output(Bool())
  // Transmit data byte
  val byte = Input(UInt(8.W))
  // Transmit request (start transmission)
  val req = Input(Bool())
  // Transmit is busy
  val busy = Output(Bool())
  // Transmit clock threshold (baud rate = clock_freq/clk_thresh)
  val clk_thresh = Input(UInt(32.W))
}

case class UartRxIO() extends Bundle {
  // Receive channel
  val rx = Input(Bool())
  // Receive data byte
  val byte = Output(Valid(UInt(8.W)))
  // Error during reception
  val error = Output(Bool())
  // Receive clock threshold (baud rate = clock_freq/clk_thresh)
  val clk_thresh = Input(UInt(32.W))
}

/** UART module */
class UART extends Module {
  val io = IO(new Bundle {
    val tx = UartTxIO()
    // Recieve channel (not functional)
    val rx = UartRxIO()
  })

  val tx_wire         = WireDefault(true.B)
  val tx_state        = RegInit(UartTxState.IDLE)
  val tx_clk_cnt_en   = tx_state =/= UartTxState.IDLE
  val tx_clk_cnt_next = Wire(UInt(32.W))
  val tx_clk_cnt      = RegEnable(next=tx_clk_cnt_next, init=0.U(32.W), enable=tx_clk_cnt_en)
  val tx_bit_stb      = tx_clk_cnt === io.tx.clk_thresh
  val tx_bit_idx      = RegInit(0.U(3.W))
  val tx_pipe         = RegInit(0.U(8.W))
  val tx_busy         = RegInit(false.B)

  tx_clk_cnt_next := tx_clk_cnt + 1.U
  when (tx_bit_stb) {
    tx_clk_cnt := 0.U
  }
  when (tx_bit_stb) {
    tx_bit_idx := tx_bit_idx + 1.U
  }

  // Transmit FSM
  when (tx_state === UartTxState.IDLE) {
    when (io.tx.req) {
      tx_busy := true.B
      tx_clk_cnt_next := 0.U
      tx_pipe := io.tx.byte
      printf("%c", io.tx.byte)  // print tx data to simulation stdout
      tx_state := UartTxState.START
    }
  } .elsewhen (tx_state === UartTxState.START) {
    tx_wire := false.B

    when (tx_bit_stb) {
      tx_bit_idx := 0.U
      tx_state := UartTxState.SHIFT_DATA
    }
  } .elsewhen (tx_state === UartTxState.SHIFT_DATA) {
    tx_wire := tx_pipe(0)
    when (tx_bit_stb) {
      tx_pipe := tx_pipe >> 1
      when (tx_bit_idx === 7.U) {
        tx_state := UartTxState.STOP
      }
    }
  } .elsewhen (tx_state === UartTxState.STOP) {
    when (tx_bit_stb) {
      tx_busy := false.B
      tx_wire := true.B
      tx_state := UartTxState.IDLE
    }
  } .otherwise {
    tx_state := UartTxState.IDLE
  }

  io.tx.tx := tx_wire
  io.tx.busy := tx_busy

  // Registers
  val state = RegInit(UartRxState.IDLE)
  val clockCounter = RegInit(0.U(16.W))
  val bitCounter = RegInit(0.U(4.W))
  val shiftReg = RegInit(0.U(8.W))
  val errorReg = RegInit(false.B)

  // Default values for outputs
  io.rx.byte.bits  := shiftReg
  io.rx.byte.valid := false.B
  io.rx.error      := errorReg

  // State Machine
  switch(state) {
    is(UartRxState.IDLE) {
      when(io.rx.rx === false.B) {
        state := UartRxState.START_BIT
        clockCounter := 0.U
      }
    }
    is(UartRxState.START_BIT) {
      when(clockCounter === (io.rx.clk_thresh >> 1)) {
        state := UartRxState.DATA_BITS
        bitCounter := 0.U
        clockCounter := 0.U
      } .otherwise {
        clockCounter := clockCounter + 1.U
      }
    }
    is(UartRxState.DATA_BITS) {
      when(clockCounter === io.rx.clk_thresh) {
        shiftReg := Cat(io.rx.rx, shiftReg(7, 1)) // Sample at each baud tick
        clockCounter := 0.U
        when(bitCounter === 7.U) {
          state := UartRxState.STOP_BIT
        } .otherwise {
          bitCounter := bitCounter + 1.U
        }
      } .otherwise {
        clockCounter := clockCounter + 1.U
      }
    }
    is(UartRxState.STOP_BIT) {
      when(clockCounter === io.rx.clk_thresh) {
        when(io.rx.rx === true.B) {
          // Valid stop bit
          state := UartRxState.IDLE
          io.rx.byte.valid := true.B
        } .otherwise {
          // Error in stop bit
          errorReg := true.B
          state := UartRxState.IDLE
        }
      } .otherwise {
        clockCounter := clockCounter + 1.U
      }
    }
  }

}

/** UART module with AXI4-Lite interface
  * Only transmit logic has been implemented.
  * Transmit side contains a FIFO - configure its depth with `tx_fifo_depth`.
  * Push data to the FIFO by writing to the `tx_byte` register.
  *
  * @param addr_width Address bus width
  * @param data_width Data bus width
  * @param tx_en Enable transmitter logic
  * @param rx_en Enable receiver logic
  * @param tx_fifo_depth Transmit FIFO depth (n of entries)
  * @param rx_fifo_depth Receiver FIFO depth (n of entries)
  */
class AXI4LUART(
  val addr_width: Int,
  val data_width: Int,
  tx_en: Boolean = false,
  rx_en: Boolean = false,
  tx_fifo_depth: Int = 8,
  rx_fifo_depth: Int = 8
  ) extends RawModule 
  with AXI4LSlave with AXI4LSlaveLogic {

  require(tx_en || rx_en, "Should enable at least one direction with `rx_en=true` and/or `tx_en=true`")

  // Inputs and outputs
  val io = IO(new Bundle {
    val tx = Output(Bool())
    val rx = Input(Bool())
  })

  val bytes = data_width / 8

  // memory-mapped registers
  val tx_clk_thresh = WireInit(0.U(32.W))          // transmit baud rate threshold
  val tx_byte       = WireInit(0.U(8.W))           // byte to be transmitted
  val tx_ctrl       = WireInit(0.U(32.W))          // transmit controls bitfield
  val tx_status     = WireInit(0.U(32.W))
  val rx_clk_thresh = WireInit(0.U(32.W))          // receive baud rate threshold
  val rx_byte       = WireInit(0.U(8.W))           // receive byte
  val rx_status     = WireInit(0.U(32.W))          // receive controls

  val uart = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {Module(new UART)}

  val tx_fifo: Option[Queue[UInt]] = if (tx_en) withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {Option(Module(new Queue(UInt(8.W), entries = tx_fifo_depth)))} else None
  val rx_fifo: Option[Queue[UInt]] = if (rx_en) withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {Option(Module(new Queue(UInt(8.W), entries = rx_fifo_depth)))} else None

  // Define only the registers that are used
  // Behavior is undefined when reading or writing to an undefined register
  val memory_map: Map[Data, MemoryRange] = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {(
    (if (tx_en) { Map[Data, MemoryRange](
      tx_clk_thresh -> MemoryRange(begin = 0, end = 3, mode = MemoryMode.RW),
      tx_byte -> MemoryRange(begin = 4, end = 7, mode = MemoryMode.W, decoupled = true, decoupled_inst = Some(tx_fifo.get.io.enq),
        name = "tx_byte", description = 
          "|Transmit data byte. Writing to this address will push it to the transmit FIFO." +
          "|Transmit starts automatically if tx_en is 1." +
          "|If the FIFO is full, the write will be stalled, if transmitter is enabled, until it is not full." +
          "|If transmitter isn't enabled, then old (not-transmitted) data is overwritten.".stripMargin),
      tx_ctrl -> MemoryRange(begin = 8, end = 11, mode = MemoryMode.RW, bitfields = List(
        BitField(begin = 0, end = 0, init = 1, name = "tx_en", description = "Enable transmitter. By default, it is enabled")
      )),
      tx_status -> MemoryRange(begin = 12, end = 15, mode = MemoryMode.R, bitfields = List(
        BitField(begin = 0, end = 0, name = "tx_busy", description = "Transmit in progress"),
        BitField(begin = 1, end = 1, name = "tx_fifo_full", description = "Transmit FIFO full"),
        BitField(begin = 2, end = 2, name = "tx_fifo_empty", description = "Transmit FIFO empty"),
      ))
    )} else Map.empty[Data, MemoryRange])

    ++

    (if (rx_en) { Map[Data, MemoryRange](
      rx_clk_thresh -> MemoryRange(begin = 16, end = 19, mode = MemoryMode.RW),
      rx_byte -> MemoryRange(begin = 20, end = 23, mode = MemoryMode.R, decoupled = true, decoupled_inst = Some(rx_fifo.get.io.deq)),
      rx_status -> MemoryRange(begin = 24, end = 27, mode = MemoryMode.R, bitfields = List(
        BitField(begin = 0, end = 0, name = "rx_fifo_empty"),
        BitField(begin = 1, end = 1, name = "rx_fifo_full"),
        BitField(begin = 2, end = 2, name = "rx_error"),
      )),
    )} else Map.empty[Data, MemoryRange])
  )}

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {

    buildAxi4LiteSlaveLogic()

    
    // Transmitter
    tx_status             := 0.U
    io.tx                 := true.B
    uart.io.tx.clk_thresh := 0.U
    uart.io.tx.byte       := 0.U
    uart.io.tx.req        := false.B
    if (tx_en) {
      io.tx                    := uart.io.tx.tx
      tx_fifo.get.io.deq.ready := !uart.io.tx.busy && tx_ctrl(0)
      uart.io.tx.clk_thresh    := tx_clk_thresh
      uart.io.tx.byte          := tx_fifo.get.io.deq.bits
      uart.io.tx.req           := tx_fifo.get.io.deq.valid && tx_ctrl(0)
      tx_status := Cat(0.U(29.W), 
                       !tx_fifo.get.io.deq.valid,                                   // tx_fifo_empty
                       !tx_fifo.get.io.enq.ready,                                   // tx_fifo_full
                       uart.io.tx.busy || (tx_fifo.get.io.deq.valid && tx_ctrl(0))) // tx_busy
    }

    // Receiver
    uart.io.rx.rx         := true.B
    rx_status             := 0.U
    uart.io.rx.clk_thresh := 0.U
    if (rx_en) {
      uart.io.rx.rx := io.rx
      rx_fifo.get.io.enq.bits  := uart.io.rx.byte.bits
      rx_fifo.get.io.enq.valid := uart.io.rx.byte.valid
      uart.io.rx.clk_thresh    := rx_clk_thresh
      rx_status := Cat(0.U(29.W),
                       !rx_fifo.get.io.deq.valid,
                       !rx_fifo.get.io.enq.ready,
                       uart.io.rx.error)
    }

  }
}

class AXI4LUARTTestWrapper extends Module {
  val io = IO(new Bundle {
    val axi = Flipped(new VerilogAXI4LIO(32, 32))
    val uart = new Bundle {
      val tx = Output(Bool())
      val rx = Input(Bool())
    }
  })
  val axi4luart = Module(new AXI4LUART(32, 32, true, true, 8, 8))
  axi4luart.clk_rst.ACLK := clock
  axi4luart.clk_rst.ARESETn := !reset.asBool
  io.uart <> axi4luart.io
  io.axi.viewAs[AXI4LIO] <> axi4luart.slave_port
}

object AXI4LUART extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LUART(addr_width = 32, data_width = 32, true, true, tx_fifo_depth = 8, rx_fifo_depth = 8)))
  (new ChiselStage).execute(args, annos)
}

object AXI4LUARTTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LUARTTestWrapper))
  (new ChiselStage).execute(args, annos)
}
