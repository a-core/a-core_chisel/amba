// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

/** Synchronous Read/Write SRAM with an AXI4-Lite port
  * @param addr_width AXI4-Lite address bus with.
  * @param data_width AXI4-Lite data bus with.
  * @param mem_depth  Address width of a single SRAM macro. AXI4LSRAM contains data_width/8 sram macros,
  *                   so the total memory size is data_width/8*2**mem_depth bytes.
  *                   Default: 12. For data_width=32 this corresponds to total size of 16KiB.
  * @param if_type    Interface type for the memory. "dual-port" or "single-port"
  */
class AXI4LSRAM(addr_width: Int, data_width: Int, mem_depth: Int = 12, if_type: String = "dual-port") extends RawModule with AXI4LSlave {
  assert(addr_width % 8 == 0, "addr_width must be a multiple of 8")
  assert(data_width % 8 == 0, "data_width must be a multiple of 8")

  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {
    val mem_count = data_width/8
    val mem_array = Seq.fill(mem_count)(SyncReadMem(scala.math.pow(2,mem_depth).intValue, UInt(8.W)))

    val sram_iface = Module(new AXI4LtoSRAM(addr_width, data_width))
    sram_iface.clk_rst <> clk_rst
    sram_iface.slave_port <> slave_port

    // word address
    val addr_offset = Log2(mem_count.U)
    val raddr_msb = sram_iface.io.read.addr >> addr_offset
    val waddr_msb = sram_iface.io.write.addr >> addr_offset

    val rdata_vec = Wire(Vec(mem_count, UInt(8.W)))
    rdata_vec := DontCare

    if (if_type == "dual-port") {
      sram_iface.io.read.gnt := sram_iface.io.read.req
      when (sram_iface.io.read.req) {
        for (i <- 0 until mem_count) {
          rdata_vec(i) := mem_array(i).read(raddr_msb)
        }
      }

      sram_iface.io.read.data.bits  := rdata_vec.asUInt
      sram_iface.io.read.data.valid := RegNext(sram_iface.io.read.req)
      sram_iface.io.read.fault      := false.B

      // write ports
      sram_iface.io.write.gnt   := sram_iface.io.write.req
      sram_iface.io.write.fault := false.B
      when (sram_iface.io.write.req) {
        for (i <- 0 until mem_count) {
          when (sram_iface.io.write.mask(i)) {
            mem_array(i).write(idx=waddr_msb, data=sram_iface.io.write.data(8*i+7, 8*i))
          }
        }
      }

    } else if (if_type == "single-port") {
      val addr = Mux(sram_iface.io.write.req, waddr_msb, raddr_msb)

      val rdwr_port_arr = mem_array.map(bank => bank(addr))
      val rdata_valid = Reg(Bool())
      rdata_valid := false.B
      sram_iface.io.read.data.valid := rdata_valid
      sram_iface.io.write.gnt := false.B
      sram_iface.io.read.gnt  := false.B
      sram_iface.io.write.fault := false.B
      sram_iface.io.read.fault := false.B

      when (sram_iface.io.write.req) {
        sram_iface.io.write.gnt := true.B
        for (i <- 0 until mem_count) {
          when (sram_iface.io.write.mask(i)) {
            rdwr_port_arr(i) := sram_iface.io.write.data(8*i+7, 8*i)
          }
        }
      } .otherwise {
        sram_iface.io.read.gnt := sram_iface.io.read.req
        rdata_valid            := sram_iface.io.read.req
        for (i <- 0 until mem_count) {
          rdata_vec(i) := rdwr_port_arr(i)
        }
      }
      sram_iface.io.read.data.bits := rdata_vec.asUInt
    } else {
      throw new NotImplementedError(s"Illegal if_type given: $if_type")
    }
  }
}

/*
/** Synchronous Read/Write SRAM with an AXI4-Lite port with delay
  * @param addr_width AXI4-Lite address bus with.
  * @param data_width AXI4-Lite data bus with.
  * @param mem_depth  Address width of a single SRAM macro. AXI4LSRAM contains data_width/8 sram macros,
  *                   so the total memory size is data_width/8*2**mem_depth bytes.
  *                   Default: 12. For data_width=32 this corresponds to total size of 16KiB.
  */
class SlowAXI4LSRAM(addr_width: Int, data_width: Int, mem_depth: Int = 12, delay_cycles: Int = 4) extends RawModule with AXI4LSlave {
  assert(addr_width % 8 == 0, "addr_width must be a multiple of 8")
  assert(data_width % 8 == 0, "data_width must be a multiple of 8")

  val clk_rst = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width=addr_width, data_width=data_width))

  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {
    val mem_count = data_width/8
    val mem_array = Seq.fill(mem_count)(SyncReadMem(scala.math.pow(2,mem_depth).intValue, UInt(8.W)))

    val sram_iface = Module(new AXI4LtoSRAM(addr_width, data_width, withStall = true))
    sram_iface.clk_rst <> clk_rst
    sram_iface.slave_port <> slave_port
    sram_iface.io.wstall.get := false.B

    // word address
    val addr_offset = Log2(mem_count.U)
    val raddr_msb = sram_iface.io.raddr >> addr_offset
    val waddr_msb = sram_iface.io.waddr >> addr_offset

    // read ports
    // connect read ports to Vec since Chisel doesn't support subword assignment
    val rdata_vec = Wire(Vec(mem_count, UInt(8.W)))
    rdata_vec := DontCare
    when (sram_iface.io.ren) {
      for (i <- 0 until mem_count) {
        rdata_vec(i) := mem_array(i).read(raddr_msb)
      }
    }
    val rOutstanding = RegInit(false.B)
    val readDelayPipe = Module(new Pipe(UInt(data_width.W), delay_cycles))
    when (sram_iface.io.ren) {
      rOutstanding := true.B
    } .elsewhen (readDelayPipe.io.deq.valid) {
      rOutstanding := false.B
    }
    readDelayPipe.io.enq.bits := rdata_vec.asUInt
    readDelayPipe.io.enq.valid := RegNext(sram_iface.io.ren)
    sram_iface.io.rdata := readDelayPipe.io.deq.bits
    sram_iface.io.rstall.get := rOutstanding && !readDelayPipe.io.deq.valid

    val wOutstanding = RegInit(false.B)
    val writeDelayPipe = Module(new Pipe(Bool(), delay_cycles))
    when (sram_iface.io.wen) {
      wOutstanding := true.B
    }.elsewhen (writeDelayPipe.io.deq.bits) {
      wOutstanding := false.B
    }
    writeDelayPipe.io.enq.bits := sram_iface.io.wen
    writeDelayPipe.io.enq.valid := true.B
    sram_iface.io.wstall.get := wOutstanding && !writeDelayPipe.io.deq.bits
    
    // write ports
    when (sram_iface.io.wen) {
      for (i <- 0 until mem_count) {
        when (sram_iface.io.wmask(i)) {
          mem_array(i).write(idx=waddr_msb, data=sram_iface.io.wdata(8*i+7, 8*i))
        }
      }
    }
  }
}

object SlowAXI4LSRAM extends App {
    val annos = Seq(ChiselGeneratorAnnotation(() => new SlowAXI4LSRAM(addr_width=32, data_width=32, mem_depth=12)))
    (new ChiselStage).execute(args, annos)
}
*/

object AXI4LSRAM extends App {
    val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LSRAM(addr_width=32, data_width=32, mem_depth=12, if_type = "single-port")))
    (new ChiselStage).execute(args, annos)
}

