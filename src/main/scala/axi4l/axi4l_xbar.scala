// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.common._
import amba.axi4l.formal._

/**
  * AXI4-Lite crossbar with integrated address decoders.
  *
  * @param addr_width
  *   Address width
  * @param data_width
  *   Data width
  * @param n_masters
  *   Number of master devices
  * @param mem_ranges
  *   List of slave addresses
  * @param arb_type
  *   Arbitration type for master arbiters. "round-robin" or "fixed"
  * @param queue_depth
  *   How deep the internal FIFOs are
  */
class AXI4LXBar(
  addr_width: Int,
  data_width: Int,
  n_masters: Int,
  mem_ranges: Seq[MemoryRange],
  arb_type: String,
  queue_depth: Int = 2
  ) extends RawModule {
  val clk_rst      = IO(new AXI4LClkRst)
  val slave_ports  = IO(Vec(n_masters, new AXI4LIO(addr_width, data_width)))
  val master_ports = IO(Vec(mem_ranges.size, Flipped(new AXI4LIO(addr_width, data_width))))

  val waddr_decoders = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn.asBool) {
    Seq.fill(n_masters)(Module(new AddressDecoder(addr_width, mem_ranges))) 
  }
  val raddr_decoders = withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn.asBool) {
    Seq.fill(n_masters)(Module(new AddressDecoder(addr_width, mem_ranges)))
  }

  val xbar = Module(new AXI4LXBarUnit(addr_width, data_width, n_masters, mem_ranges.size, arb_type))

  clk_rst      <> xbar.clk_rst
  slave_ports  <> xbar.slave_ports
  master_ports <> xbar.master_ports

  for (i <- 0 until n_masters) {
    raddr_decoders(i).io.en          := true.B
    raddr_decoders(i).io.addr        := slave_ports(i).AR.bits.ADDR
    waddr_decoders(i).io.en          := true.B
    waddr_decoders(i).io.addr        := slave_ports(i).AW.bits.ADDR
    xbar.r_idxs(i)                   := raddr_decoders(i).io.index
    xbar.w_idxs(i)                   := waddr_decoders(i).io.index
    xbar.slave_ports(i).AR.bits.ADDR := raddr_decoders(i).io.addr_local
    xbar.slave_ports(i).AW.bits.ADDR := waddr_decoders(i).io.addr_local
  }
}


/**
  * AXI4-Lite crossbar. Does not contain address decoders.
  *
  * @param addr_width
  *   Address width
  * @param data_width
  *   Data width
  * @param n_masters
  *   Number of master devices
  * @param n_slaves
  *   Number of slave devices
  * @param arb_type
  *   Arbitration type for master arbiters. "round-robin" or "fixed"
  * @param queue_depth
  *   How deep the internal FIFOs are
  */
class AXI4LXBarUnit(
  addr_width: Int,
  data_width: Int,
  n_masters: Int,
  n_slaves: Int,
  arb_type: String,
  queue_depth: Int = 2
  ) extends RawModule {
  val idx_width    = log2Ceil(n_slaves)
  val clk_rst      = IO(new AXI4LClkRst)
  val slave_ports  = IO(Vec(n_masters, new AXI4LIO(addr_width, data_width)))
  val r_idxs       = IO(Vec(n_masters, Input(UInt(idx_width.W))))
  val w_idxs       = IO(Vec(n_masters, Input(UInt(idx_width.W))))
  val master_ports = IO(Vec(n_slaves, Flipped(new AXI4LIO(addr_width, data_width))))

  val master_demuxes = Seq.fill(n_masters)(Module(new AXI4LMMDemuxUnit(addr_width, data_width, n_slaves)))
  val slave_arbiters = Seq.fill(n_slaves)(Module(new AXI4LMasterArbiter(n_masters, addr_width, data_width, arb_type, queue_depth)))

  master_demuxes.foreach(_.clk_rst <> clk_rst)
  slave_arbiters.foreach(_.clk_rst <> clk_rst)

  withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {

    // Connect Demuxes to slave ports
    master_demuxes.zip(slave_ports).map { case (a, b) => a.slave_port <> b }
    master_demuxes.zip(r_idxs).map { case (a, b) => a.r_idx := b}
    master_demuxes.zip(w_idxs).map { case (a, b) => a.w_idx := b}

    // Connect Demux master ports to arbiter slave ports
    for (i <- 0 until n_slaves) {
      for (j <- 0 until n_masters) {
        slave_arbiters(i).slave_ports(j) <> master_demuxes(j).master_ports(i)
      }
    }

    // Connect arbiter outputs to master ports
    slave_arbiters.zip(master_ports).map { case (a, b) => a.master_port <> b }

  }

}

/** Thin test wrapper module for formal verification. This module instantiates the interconnect DUT and connects it to
  * formal master and slave devices. The formal master constrains the slave port of the interconnect to only generate
  * legal transactions, while each master port is connected to a formal slave module with liveness and safety
  * properties.
  *
  * @param addr_width
  *   address bus width
  * @param data_width
  *   data bus width
  * @param mem_ranges
  *   Seq of memory ranges corresponding to memory ranges of connected slaves.
  */
class AXI4LXBarTestWrapper(addr_width: Int, data_width: Int, n_masters: Int, mem_ranges: Seq[MemoryRange])
    extends RawModule {
  val clk_rst = IO(new AXI4LClkRst)

  // Create the memory_map data struct you would have on normal Chisel generators (using Formal modules)
  val formal_masters = Seq.fill(n_masters)(Module(new AXI4LFormalMaster(addr_width, data_width)))
  val memory_map     = mem_ranges.map(x => x -> Module(new AXI4LFormalSlave(addr_width, data_width))).toMap

  // Instantiate interconnect DUT
  val axi4l_xbar = Module(new AXI4LXBar(addr_width, data_width, n_masters, mem_ranges, "round-robin"))

  axi4l_xbar.clk_rst <> clk_rst

  // Connect formal master and slaves to DUT
  formal_masters.foreach(_.clk_rst <> clk_rst)
  formal_masters.zip(axi4l_xbar.slave_ports).map { case (a, b) => a.master_port <> b }

  for (((range, slave), i) <- memory_map.zipWithIndex) {
    slave.clk_rst <> clk_rst
    slave.slave_port <> axi4l_xbar.master_ports(i)
  }

}

/** Renames AXI signal so that they can be used by cocotbext-axi
  *
  * @param addr_width
  *   address bus width
  * @param data_width
  *   data bus width
  * @param mem_ranges
  *   Seq of memory ranges corresponding to memory ranges of connected slaves.
  */
class AXI4LXBarCocotbWrapper(addr_width: Int, data_width: Int, n_masters: Int, mem_ranges: Seq[MemoryRange])
    extends RawModule {

  val axi_master_io = IO(Vec(
    n_masters,
    new Bundle {
      // Clock and Reset
      val aclk    = Input(Clock())
      val aresetn = Input(Bool())
      // Read Address Channel
      val arready = Output(Bool())
      val arvalid = Input(Bool())
      val araddr  = Input(UInt(addr_width.W))
      val arprot  = Input(UInt(3.W))
      // Read Data Channel
      val rready = Input(Bool())
      val rvalid = Output(Bool())
      val rdata  = Output(UInt(data_width.W))
      val rresp  = Output(Response())
      // Write Address Channel
      val awready = Output(Bool())
      val awvalid = Input(Bool())
      val awaddr  = Input(UInt(addr_width.W))
      val awprot  = Input(UInt(3.W))
      // Write Data Channel
      val wready = Output(Bool())
      val wvalid = Input(Bool())
      val wdata  = Input(UInt(data_width.W))
      val wstrb  = Input(UInt((data_width / 8).W))
      // Write Response Channel
      val bready = Input(Bool())
      val bvalid = Output(Bool())
      val bresp  = Output(Response())
    }
  ))

  val axi_slave_io = IO(Vec(
    mem_ranges.size,
    new Bundle {
      // Clock and Reset
      val aclk    = Input(Clock())
      val aresetn = Input(Bool())
      // Read Address Channel
      val arready = Input(Bool())
      val arvalid = Output(Bool())
      val araddr  = Output(UInt(addr_width.W))
      val arprot  = Output(UInt(3.W))
      // Read Data Channel
      val rready = Output(Bool())
      val rvalid = Input(Bool())
      val rdata  = Input(UInt(data_width.W))
      val rresp  = Input(Response())
      // Write Address Channel
      val awready = Input(Bool())
      val awvalid = Output(Bool())
      val awaddr  = Output(UInt(addr_width.W))
      val awprot  = Output(UInt(3.W))
      // Write Data Channel
      val wready = Input(Bool())
      val wvalid = Output(Bool())
      val wdata  = Output(UInt(data_width.W))
      val wstrb  = Output(UInt((data_width / 8).W))
      // Write Response Channel
      val bready = Output(Bool())
      val bvalid = Input(Bool())
      val bresp  = Input(Response())
    }
  ))

  // Instantiate interconnect DUT
  val axi4l_xbar = Module(new AXI4LXBar(addr_width, data_width, n_masters, mem_ranges, "round-robin"))

  axi4l_xbar.clk_rst.ACLK    := axi_master_io(0).aclk
  axi4l_xbar.clk_rst.ARESETn := axi_master_io(0).aresetn

  for (i <- 0 until n_masters) {
    axi_master_io(i).arready := axi4l_xbar.slave_ports(i).AR.ready
    axi_master_io(i).rvalid  := axi4l_xbar.slave_ports(i).R.valid
    axi_master_io(i).rdata   := axi4l_xbar.slave_ports(i).R.bits.DATA
    axi_master_io(i).rresp   := axi4l_xbar.slave_ports(i).R.bits.RESP
    axi_master_io(i).awready := axi4l_xbar.slave_ports(i).AW.ready
    axi_master_io(i).wready  := axi4l_xbar.slave_ports(i).W.ready
    axi_master_io(i).bvalid  := axi4l_xbar.slave_ports(i).B.valid
    axi_master_io(i).bresp   := axi4l_xbar.slave_ports(i).B.bits.RESP

    axi4l_xbar.slave_ports(i).AR.valid     := axi_master_io(i).arvalid
    axi4l_xbar.slave_ports(i).AR.bits.ADDR := axi_master_io(i).araddr
    axi4l_xbar.slave_ports(i).AR.bits.PROT := axi_master_io(i).arprot
    axi4l_xbar.slave_ports(i).R.ready      := axi_master_io(i).rready
    axi4l_xbar.slave_ports(i).AW.valid     := axi_master_io(i).awvalid
    axi4l_xbar.slave_ports(i).AW.bits.ADDR := axi_master_io(i).awaddr
    axi4l_xbar.slave_ports(i).AW.bits.PROT := axi_master_io(i).awprot
    axi4l_xbar.slave_ports(i).W.valid      := axi_master_io(i).wvalid
    axi4l_xbar.slave_ports(i).W.bits.DATA  := axi_master_io(i).wdata
    axi4l_xbar.slave_ports(i).W.bits.STRB  := axi_master_io(i).wstrb
    axi4l_xbar.slave_ports(i).B.ready      := axi_master_io(i).bready
  }

  for (i <- 0 until mem_ranges.size) {

    axi4l_xbar.master_ports(i).AR.ready    := axi_slave_io(i).arready
    axi4l_xbar.master_ports(i).R.valid     := axi_slave_io(i).rvalid
    axi4l_xbar.master_ports(i).R.bits.DATA := axi_slave_io(i).rdata
    axi4l_xbar.master_ports(i).R.bits.RESP := axi_slave_io(i).rresp
    axi4l_xbar.master_ports(i).AW.ready    := axi_slave_io(i).awready
    axi4l_xbar.master_ports(i).W.ready     := axi_slave_io(i).wready
    axi4l_xbar.master_ports(i).B.valid     := axi_slave_io(i).bvalid
    axi4l_xbar.master_ports(i).B.bits.RESP := axi_slave_io(i).bresp

    axi_slave_io(i).arvalid := axi4l_xbar.master_ports(i).AR.valid
    axi_slave_io(i).araddr  := axi4l_xbar.master_ports(i).AR.bits.ADDR
    axi_slave_io(i).arprot  := axi4l_xbar.master_ports(i).AR.bits.PROT
    axi_slave_io(i).rready  := axi4l_xbar.master_ports(i).R.ready
    axi_slave_io(i).awvalid := axi4l_xbar.master_ports(i).AW.valid
    axi_slave_io(i).awaddr  := axi4l_xbar.master_ports(i).AW.bits.ADDR
    axi_slave_io(i).awprot  := axi4l_xbar.master_ports(i).AW.bits.PROT
    axi_slave_io(i).wvalid  := axi4l_xbar.master_ports(i).W.valid
    axi_slave_io(i).wdata   := axi4l_xbar.master_ports(i).W.bits.DATA
    axi_slave_io(i).wstrb   := axi4l_xbar.master_ports(i).W.bits.STRB
    axi_slave_io(i).bready  := axi4l_xbar.master_ports(i).B.ready

  }

}

object AXI4LXBarTestWrapper extends App {

  val mem_ranges = Seq(
    MemoryRange(begin = "h0000_1000", end = "h0000_1FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_2000", end = "h0000_2FFF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_3000", end = "h0000_3FFF", mode = MemoryMode.RW)
  )

  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LXBarTestWrapper(32, 32, 2, mem_ranges)))
  (new ChiselStage).execute(args, annos)
}

object AXI4LXBarCocotbWrapper extends App {

  val mem_ranges = Seq(
    MemoryRange(begin = "h0000_1000", end = "h0000_10FF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_2000", end = "h0000_20FF", mode = MemoryMode.RW),
    MemoryRange(begin = "h0000_3000", end = "h0000_30FF", mode = MemoryMode.RW)
  )

  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LXBarCocotbWrapper(32, 32, 2, mem_ranges)))
  (new ChiselStage).execute(args, annos)
}
