// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import amba.common.{MemoryRange, MemoryMode}
import amba.common.SkidBuffer
import amba.axi4l.formal.AXI4LFormalMaster

/** Adds AXI4-Lite slave logic for a Chisel module. Defines two IOs: `clk_reset` (clock and reset for AXI4Lite), and
  * `slave_port` (holds AXI4Lite signals). The module which extends this trait must define `memory_map`, `addr_width`,
  * and `data_width`. To use: add `with AXI4LSlaveLogic` to your class definition, define the aforementioned, call
  * `buildAxi4LiteSlaveLogic`.
  * NOTE: Assumes 32-bit wide registers
  */
trait AXI4LSlaveLogic {

  /* THESE VALUES MUST BE PROVIDED BY THE MODULE THAT EXTENDS THIS TRAIT */

  // Memory map of the slave
  def memory_map: Map[Data, MemoryRange]
  // Width of the address bus
  def addr_width: Int
  // Width of the data bus
  def data_width: Int

  /* END */

  // How many words per register
  val reg_size  = 4
  // How many bits per register
  val word_size = reg_size * 8

  // AXI4Lite clock and reset
  val clk_rst = IO(new AXI4LClkRst)
  // AXI4Lite IOs
  val slave_port = IO(new AXI4LIO(addr_width, data_width))

  /**
    * Builds AXI4Lite Slave logic based on the defined memory map
    *
    */
  def buildAxi4LiteSlaveLogic() = {

    // Convert Map to a sorted Seq of tuples
    val sorted_mmap = memory_map.toSeq.sortBy(_._2.begin)
    // Create a map that tells the index of the register where a memory address is stored
    val index_map = sorted_mmap.zipWithIndex.map { case (value, index) => (value._2.begin, index) }.toMap

    withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {

      // Vector of AXI registers, initialized to the given value
      val axi_regs = RegInit(VecInit.tabulate(sorted_mmap.length) {
        idx => {
          val memrange = sorted_mmap(idx)._2
          sorted_mmap(idx)._2.initChisel
        }
      })


      // Iterate over all entries in the memory map and connect IOs
      for (i <- 0 until sorted_mmap.length) {
        val (wire, memory_range) = sorted_mmap(i)
        if (memory_range.mode == MemoryMode.R) {
          when (memory_range.write_en_sig) {
            axi_regs(i) := wire
          }
        } else if (memory_range.mode == MemoryMode.RW || memory_range.mode == MemoryMode.W) {
          wire := axi_regs(i)
          // Implement autoClear
          if (memory_range.bitfields.length > 0) {
            val masks = Wire(Vec(memory_range.bitfields.length, UInt(data_width.W)))
            val shiftVals = Wire(Vec(memory_range.bitfields.length, UInt(data_width.W)))
            for ((bitfield, j) <- memory_range.bitfields.zipWithIndex) {
              if (bitfield.autoClear) {
                masks(j)     := ~bitfield.mask.U(word_size.W)
                shiftVals(j) := bitfield.shiftedVal.U(word_size.W)
              } else {
                masks(j)     := Fill(word_size, 1.U)
                shiftVals(j) := 0.U(word_size.W)
              }
            }
            val combinedMask = masks.reduce(_ & _)
            val combinedVal = shiftVals.reduce(_ | _)
            axi_regs(i) := (axi_regs(i) & combinedMask) | combinedVal
          }
        }
      }

      /*------------------- READ PORT ------------------- */

      val ar_buf = Module(new SkidBuffer(new AR(addr_width)))
      val r_buf  = Module(new SkidBuffer(new R(data_width), cut = true))

      // Drop 2 LSBs
      val raddr = Cat(ar_buf.io.out.bits.ADDR(addr_width-1, 2), 0.U(2.W))

      slave_port.AR.ready    := ar_buf.io.in.ready
      slave_port.R.bits      := r_buf.io.out.bits
      slave_port.R.valid     := r_buf.io.out.valid
      r_buf.io.out.ready     := slave_port.R.ready

      ar_buf.io.in.bits   := slave_port.AR.bits
      ar_buf.io.in.valid  := slave_port.AR.valid
      ar_buf.io.out.ready := false.B

      val readStall         = WireDefault(false.B)
      val readData          = WireDefault(0.U(32.W))
      // Not used for read channel for the moment
      val readAccessStrobes = RegNext(VecInit.fill(sorted_mmap.length) {WireDefault(false.B)})
      val readStrobes       = RegNext(VecInit.fill(sorted_mmap.length) {WireDefault(false.B)})

      val index_addr = WireDefault(0.U)

      for ((memory_range, n) <- sorted_mmap.zipWithIndex) {
        if (memory_range._2.mode == MemoryMode.R || memory_range._2.mode == MemoryMode.RW) {
          val address = memory_range._2.begin
          memory_range._2.readStrobe = readStrobes(n)
          memory_range._2.readAccess = readAccessStrobes(n)
          if (memory_range._2.decoupled) {
            val decoupled_inst = memory_range._2.decoupled_inst.get
            decoupled_inst.ready := false.B
            when(raddr === address.U) {
              decoupled_inst.ready := r_buf.io.in.ready
              ar_buf.io.out.ready    := decoupled_inst.fire
              readStall              := !decoupled_inst.valid
              readData               := decoupled_inst.bits
            }
          } else { // reg
            when(raddr === address.U) {
              ar_buf.io.out.ready  := !readStall && r_buf.io.in.ready
              readStall            := memory_range._2.read_stall_sig
              index_addr           := index_map(address).U
              readData             := axi_regs(index_addr)
            }
          }
        }
      }
      r_buf.io.in.valid     := ar_buf.io.out.valid && !readStall
      r_buf.io.in.bits.DATA := readData
      r_buf.io.in.bits.RESP := Response.OKAY
      
      /*------------------- WRITE PORT ------------------ */

      val aw_buf = Module(new SkidBuffer(new AW(addr_width)))
      val w_buf  = Module(new SkidBuffer(new W(data_width)))
      val b_buf  = Module(new SkidBuffer(new B, cut = true))

      val wStall = WireDefault(false.B)

      val waddr = Cat(aw_buf.io.out.bits.ADDR(addr_width-1, 2), 0.U(2.W))

      val wdata = w_buf.io.out.bits.DATA
      val wstrb = w_buf.io.out.bits.STRB

      slave_port.AW.ready    := aw_buf.io.in.ready
      slave_port.W.ready     := w_buf.io.in.ready
      slave_port.B.valid     := b_buf.io.out.valid
      slave_port.B.bits.RESP := b_buf.io.out.bits.RESP
      aw_buf.io.in.bits      := slave_port.AW.bits
      aw_buf.io.in.valid     := slave_port.AW.valid
      w_buf.io.in.valid      := slave_port.W.valid
      w_buf.io.in.bits       := slave_port.W.bits
      b_buf.io.in.valid      := false.B
      b_buf.io.in.bits.RESP  := Response.DECERR
      b_buf.io.out.ready     := slave_port.B.ready
      aw_buf.io.out.ready    := false.B
      w_buf.io.out.ready     := false.B

      val wr_reg_index       = WireDefault(0.U)
      val wr_address_match   = WireDefault(false.B)
      val wr_en              = WireDefault(false.B)
      val writeAccessStrobes = RegNext(VecInit.fill(sorted_mmap.length) {WireDefault(false.B)})
      val writeStrobes       = RegNext(VecInit.fill(sorted_mmap.length) {WireDefault(false.B)})

      val addr_data_valid = aw_buf.io.out.valid && w_buf.io.out.valid

      // Create a match signal and generate vector index for axi reg
      // Do not create them for registers that do not have write access
      for ((memory_range, n) <- sorted_mmap.zipWithIndex) {
        if (memory_range._2.mode == MemoryMode.RW || memory_range._2.mode == MemoryMode.W) {
          val address = memory_range._2.begin
          memory_range._2.writeStrobe = writeStrobes(n)
          memory_range._2.writeAccess = writeAccessStrobes(n)
          if (memory_range._2.decoupled) {
            val decoupled_inst = memory_range._2.decoupled_inst.get
            decoupled_inst.bits  := w_buf.io.out.bits.DATA
            decoupled_inst.valid := false.B
            when((waddr === address.U) && aw_buf.io.out.valid) {
              decoupled_inst.valid   := true.B
              wStall                 := !decoupled_inst.ready
              wr_address_match       := true.B
              wr_en                  := false.B
            }
          } 
          else {
            when(waddr === address.U) {
              wStall                := memory_range._2.write_stall_sig
              wr_reg_index          := index_map(address).U
              wr_address_match      := true.B
              wr_en                 := true.B
              writeAccessStrobes(n) := true.B
            }
          }
        }
      }

      // Edit only the bytes defined by wmask
      val new_out = Wire(Vec(reg_size, UInt(8.W)))
      for (i <- 0 until reg_size) {
        when(wstrb(i) === 1.U) {
          new_out(i) := wdata((i + 1) * 8 - 1, i * 8)
        }.otherwise {
          new_out(i) := axi_regs(wr_reg_index)((i + 1) * 8 - 1, i * 8)
        }
      }

      when(addr_data_valid && !wStall) {
        aw_buf.io.out.ready        := b_buf.io.in.ready
        w_buf.io.out.ready         := b_buf.io.in.ready
        b_buf.io.in.valid          := true.B
        when(wr_address_match) {
          writeStrobes(wr_reg_index) := b_buf.io.in.ready
          b_buf.io.in.bits.RESP      := Response.OKAY
          when (wr_en) {
            axi_regs(wr_reg_index) := new_out.reduce((a, b) => Cat(b, a))
          }
        }
      }
    }
  }
}

class AXI4LSlaveLogicTest(
  val addr_width: Int = 32,
  val data_width: Int = 32
) extends RawModule with AXI4LSlaveLogic {
  val io = IO(new Bundle {
    val in = Input(UInt(32.W))
    val out = Output(UInt(32.W))
  })

  val rStallCnt = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {new Counter(10) }
  val rCounterWrap = rStallCnt.value === (rStallCnt.n - 1).U
  val rCounting = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) { RegInit(false.B) }
  when (rCounting) {
    rStallCnt.inc()
  }

  val wStallCnt = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) {new Counter(10) }
  val wCounterWrap = wStallCnt.value === (wStallCnt.n - 1).U
  val wCounting = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn) { RegInit(false.B) }
  when (wCounting) {
    wStallCnt.inc()
  }

  val reg1 = Wire(UInt(32.W))
  val reg2 = Wire(UInt(32.W))
  val memory_map = Map[Data, MemoryRange](
    reg1 -> MemoryRange(begin = "h0000_0000", end = "h0000_0003", mode = MemoryMode.R, read_stall_sig = !rCounterWrap),
    reg2 -> MemoryRange(begin = "h0000_0010", end = "h0000_0013", mode = MemoryMode.RW, write_stall_sig = !wCounterWrap)
  )
  buildAxi4LiteSlaveLogic()

  when (memory_map(reg1).readAccess) {
    rCounting := true.B
  } .elsewhen (wCounterWrap) {
    rCounting := false.B
  }

  when (memory_map(reg2).writeAccess) {
    wCounting := true.B
  } .elsewhen (wCounterWrap) {
    wCounting := false.B
  }

  reg1 := io.in
  io.out := reg2
}

class AXI4LSlaveLogicFormalTestWrapper extends RawModule {
  val clk_rst = IO(new AXI4LClkRst)
  val axi4l_slave = Module(new AXI4LSlaveLogicTest(32,32))
  val formal_master = Module(new AXI4LFormalMaster(32,32))

  formal_master.clk_rst <> clk_rst
  axi4l_slave.clk_rst <> clk_rst
  formal_master.master_port <> axi4l_slave.slave_port
  axi4l_slave.io.in := 5.U
}

object AXI4LSlaveLogicFormalTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LSlaveLogicFormalTestWrapper))
  (new ChiselStage).execute(args, annos)
}
