// SPDX-License-Identifier: Apache-2.0

package amba.axi4l

import chisel3._
import chisel3.util._
import chisel3.experimental._
import amba.common._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

class GPIO(input_width: Int, output_width: Int, withSync: Boolean = true) extends Module {

  val io = IO(new Bundle {

    val external = new Bundle {

      // Inputs coming from outside world
      val in_vec = Input(UInt(input_width.W))
      // Outputs towards outside world
      val out_vec = Output(UInt(output_width.W))
    }

    val internal = new Bundle {

      // Synchronized input data towards the processor
      val in_data = Output(UInt(input_width.W))
      // Output data from the processor
      val out_data = Input(UInt(output_width.W))
    }

  })

  // Optional synchronizer register for input data
  val in_reg_cdc_reg = if (withSync) RegNext(io.external.in_vec) else RegInit(0.U(input_width.W))
  val in_reg         = if (withSync) RegNext(in_reg_cdc_reg) else io.external.in_vec

  // Output data simply fed through
  io.external.out_vec := io.internal.out_data
  io.internal.in_data := in_reg
}

class AXI4LGPIO(
    val addr_width: Int = 32,
    val data_width: Int = 32,
    withSync: Boolean = true,
    input_width: Int = 32,
    output_width: Int = 32
) extends RawModule
    with AXI4LSlaveLogic with AXI4LSlave {
  
  val gpio = withClockAndReset(clk_rst.ACLK, ~clk_rst.ARESETn){ Module(new GPIO(input_width, output_width, withSync)) }
  val io   = IO(chiselTypeOf(gpio.io.external))

  // Memory map for input and output registers
  val memory_map = Map[Data, MemoryRange](
    gpio.io.internal.in_data  -> MemoryRange(begin = "h0000_0000", end = "h0000_0003", mode = MemoryMode.R, name = "IN_REG"),
    gpio.io.internal.out_data -> MemoryRange(begin = "h0000_0010", end = "h0000_0013", mode = MemoryMode.RW, name = "OUT_REG")
  )
  buildAxi4LiteSlaveLogic()

  io <> gpio.io.external
}

/** Test wrapper for AXI4LGPIO. Renames AXI4L signals to standard format.
  * @param addr_width
  *   Address bus width
  * @param data_width
  *   Data bus width
  * @param XLEN
  *   Global register width
  */
class AXI4LGPIOTestWrapper(addr_width: Int, data_width: Int, input_width: Int = 32, output_width: Int = 32)
    extends RawModule {
  val axi4gpio = Module(new AXI4LGPIO(addr_width, data_width, true, input_width, output_width))

  val io_io = IO(new Bundle {
    val in_vec  = Input(UInt(input_width.W))
    val out_vec = Output(UInt(output_width.W))
  })

  val axi_io = IO(new Bundle {
    // Clock and Reset
    val aclk    = Input(Clock())
    val aresetn = Input(Bool())
    // Read Address Channel
    val arready = Output(Bool())
    val arvalid = Input(Bool())
    val araddr  = Input(UInt(addr_width.W))
    val arprot  = Input(UInt(3.W))
    // Read Data Channel
    val rready = Input(Bool())
    val rvalid = Output(Bool())
    val rdata  = Output(UInt(data_width.W))
    val rresp  = Output(Response())
    // Write Address Channel
    val awready = Output(Bool())
    val awvalid = Input(Bool())
    val awaddr  = Input(UInt(addr_width.W))
    val awprot  = Input(UInt(3.W))
    // Write Data Channel
    val wready = Output(Bool())
    val wvalid = Input(Bool())
    val wdata  = Input(UInt(data_width.W))
    val wstrb  = Input(UInt((data_width / 8).W))
    // Write Response Channel
    val bready = Input(Bool())
    val bvalid = Output(Bool())
    val bresp  = Output(Response())
  })

  axi_io.arready := axi4gpio.slave_port.AR.ready
  axi_io.rvalid  := axi4gpio.slave_port.R.valid
  axi_io.rdata   := axi4gpio.slave_port.R.bits.DATA
  axi_io.rresp   := axi4gpio.slave_port.R.bits.RESP
  axi_io.awready := axi4gpio.slave_port.AW.ready
  axi_io.wready  := axi4gpio.slave_port.W.ready
  axi_io.bvalid  := axi4gpio.slave_port.B.valid
  axi_io.bresp   := axi4gpio.slave_port.B.bits.RESP

  axi4gpio.clk_rst.ACLK            := axi_io.aclk
  axi4gpio.clk_rst.ARESETn         := axi_io.aresetn
  axi4gpio.slave_port.AR.valid     := axi_io.arvalid
  axi4gpio.slave_port.AR.bits.ADDR := axi_io.araddr
  axi4gpio.slave_port.AR.bits.PROT := axi_io.arprot
  axi4gpio.slave_port.R.ready      := axi_io.rready
  axi4gpio.slave_port.AW.valid     := axi_io.awvalid
  axi4gpio.slave_port.AW.bits.ADDR := axi_io.awaddr
  axi4gpio.slave_port.AW.bits.PROT := axi_io.awprot
  axi4gpio.slave_port.W.valid      := axi_io.wvalid
  axi4gpio.slave_port.W.bits.DATA  := axi_io.wdata
  axi4gpio.slave_port.W.bits.STRB  := axi_io.wstrb
  axi4gpio.slave_port.B.ready      := axi_io.bready

  io_io.out_vec      := axi4gpio.io.out_vec
  axi4gpio.io.in_vec := io_io.in_vec
}

object AXI4LGPIOTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LGPIOTestWrapper(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}
