// SPDX-License-Identifier: Apache-2.0

package amba.common

import chisel3._
import chisel3.util._
import chisel3.experimental._
import scala.collection.immutable.ListMap
import java.io._
import java.util.Calendar
import scala.reflect.{ClassTag, classTag}

/**
  * Use this trait to generate C header file for an AXI4L slave using genCHeaders()
  */
trait genCHeaders {
  /**
    * Print C headers for a given slave memory map
    * Memory map must be in format Map[T, MemoryRange], where
    * T can be chisel3.Data or String
    * By default, stored in axi4l-slave-mmap.h
    *
    * @param memoryMap memory map in the correct format
    * @param file file name for header file
    * @param moduleName name of the slave module, used to generate include guard name
    * @param printToTerminal whether to print result to terminal
    */

  def genCHeaders[T: ClassTag,U: ClassTag](
    memoryMap : Map[T, U],
    file : String,
    moduleName : String,
    printToTerminal : Boolean
  ) : Unit = {
    // Checks whether MemoryRange is the key or the value
    // Swaps before sending to genCHeadersToFile if necessary
    // @unchecked suppresses warnings
    // For classTag, see https://stackoverflow.com/a/21640639
    val newMemoryMap : Map[_, MemoryRange] = memoryMap match {
      case mm: Map[_, MemoryRange] @unchecked if classTag[U] == classTag[MemoryRange] => mm
      case mm: Map[MemoryRange, _] @unchecked if classTag[T] == classTag[MemoryRange] => (Map() ++ mm.map(_.swap))
      case _ => throw new RuntimeException(
        "Unsupported type for memoryMap: either key or value must be MemoryRange!"
        )
    }
    genCHeadersToFile(newMemoryMap, file, moduleName, printToTerminal)
  }

  /**
    * Print C headers for a given slave memory map
    * Memory map must be in format Seq[MemoryRange]
    *
    * @param memoryMap
    * @param file
    * @param moduleName
    * @param printToTerminal
    */
  def genCHeaders(
    memoryMap : Seq[MemoryRange],
    file : String,
    moduleName : String,
    printToTerminal : Boolean
  ) : Unit = {
    val dummyKeys = memoryMap.map(_.name).toSeq
    val newMemoryMap: Map[String, MemoryRange] = dummyKeys.zip(memoryMap).toMap
    genCHeadersToFile(newMemoryMap, file, moduleName, printToTerminal)
  }


  private def genCHeadersToFile[T](
    memoryMap : Map[T, MemoryRange],
    file : String,
    moduleName : String,
    printToTerminal : Boolean
    ) : Unit = {

    def keyToString[T](key : T) : String = {
      // If key is a Chisel Data instance, convert to string
      val keyName : String = key match {
        case kn: Data => kn.instanceName
        case kn: String => kn
        case kn: BaseModule => kn.name
        case unmatch => throw new RuntimeException("Unsupported type for memoryMap! " + unmatch.getClass)
      }
      keyName
    }

    (new File((new File(file)).getParent)).mkdirs()
    val pw = new PrintWriter(new File(file))
    val date = Calendar.getInstance()
    val headerName = moduleName.toUpperCase() + "_MMAP_H"

    // Sorts Map, which is in random order by default
    val sortedMmap = ListMap(memoryMap.toSeq.sortBy(_._2.begin):_*)

    // Find longest key length for formatting
    // All names have moduleName as prefix
    // Added by 5 as initial value has _INIT postfix
    // NOTE: this check does not check custom names
    val longestKeyLength = sortedMmap.keySet
      .map(keyToString(_))
      .maxBy(_.length)
      .length + 5

    def writeRow(writeString: String) = {
      pw.write(writeString)
      if (printToTerminal) print(writeString)
    }

    def generateRow(moduleName: String, extension: String, value: BigInt) = {
      val genRow = "#define %%s_%%-%ds 0x%%-8x\n".format(longestKeyLength)
      .format(moduleName.toUpperCase(), extension.toUpperCase(), value)
      writeRow(genRow)
    }

    var startGuard = ""
    startGuard += "// Automatically generated " + date.getTime() + "\n\n"
    startGuard += f"#ifndef $headerName\n"
    startGuard += f"#define $headerName\n\n"
    if (printToTerminal) {
      println(f"File: $file")
      println(startGuard)
    }
    pw.write(startGuard)

    for ((key, memoryRange) <- sortedMmap) {
      if (memoryRange.description != "") {
        writeRow("/*\n")
        writeRow(memoryRange.description)
        writeRow("\n*/\n")
      }
      // example: adc[5] -> adc_5
      val fieldName = 
        memoryRange.name
        .replace(".", "_")
        .replace("[", "_")
        .replace("]", "")
        .toUpperCase()
      val keyName = {
        if (memoryRange.name != "") fieldName
        else                        keyToString(key)
      }
      generateRow(moduleName, keyName, memoryRange.begin)
      if (memoryRange.init != 0) {
        generateRow(moduleName, (keyName + "_INIT"), memoryRange.init)
      }
      for (bitfield <- memoryRange.bitfields) {
        if (bitfield.description != "") {
          writeRow("/*\n")
          writeRow(bitfield.description)
          writeRow("\n*/\n")
        }
        val bitFieldName = keyName + "_" + {
          if (bitfield.name != "") bitfield.name
          else                     bitfield.begin.toString() + "_" + bitfield.end.toString()
        }
        generateRow(moduleName, (bitFieldName + "_OFFSET"), bitfield.begin)
        generateRow(moduleName, (bitFieldName + "_MASK"), bitfield.mask)
        if (bitfield.init != 0) {
          generateRow(moduleName, (bitFieldName + "_INIT"), bitfield.init)
        }
      }
      pw.write("\n")
    }
    var endGuard = f"\n#endif /* $headerName */"
    writeRow(endGuard)
    pw.close()
  }
}

/** Enumeration of memory access modes
  *
  * R  - read only
  * W  - write only
  * RW - read/write
  */
object MemoryMode extends Enumeration {
  type MemoryMode = Value
  val R  = Value
  val W  = Value
  val RW = Value
}

/**
  * Type for representing bitfields inside a MemoryRange.
  * Allows setting initial values and auto-clear functionality.
  * Auto-clear means that the bit will be set to its init value at the next rising edge.
  * This allows creating e.g. reset pulses with one write transaction.
  * Note: assumes 32-bit register width
  *
  * @param begin Begin bit index of the bitfield (0 = LSB) (inclusive)
  * @param end End bit index of the bitfield (0 = LSB) (inclusive)
  * @param init Initial value
  * @param autoClear Enable Auto clear
  * @param name Custom name, used in C headers. This name is appended to the name of MemoryRange.
  * @param description Custom description to help SW developers
  */
class BitField(val begin: Int,
               val end: Int,
               val init: BigInt = 0,
               val autoClear: Boolean = false,
               val name: String = "",
               val description: String = "") {
  require(begin >= 0, "begin must be >= 0")
  require(end >= 0, "end must be >= 0")
  require(begin <= end, "begin must be <= end")
  val mask: BigInt = ((BigInt(1) << (end - begin + 1)) - 1) << begin
  val shiftedVal: BigInt = init << begin
}
object BitField {
  def apply(begin: Int,
            end: Int,
            init: BigInt = 0,
            autoClear: Boolean = false,
            name: String = "",
            description: String = "") = new BitField(begin, end, init, autoClear, name, description)
}

/** Type for representing memory ranges for memory mapped IO.
  * You need to define `begin` and either `end` or `depth`.
  * The remaining parameters are optional.
  *
  * @param begin First address of the memory range (inclusive)
  * @param end End address of the memory range (inclusive)
  * @param depth Depth of the memory range (size = 2^depth)
  * @param mode Access mode of the memory region as indicated by [[MemoryMode]] 
  * @param cached Whether the memory range can be cached
  * @param init Default value for the memory. A BitField initial value will override their part.
  * @param decoupled Target utilizes a decoupled interface (e.g. a FIFO's enq or deq IO)
  * @param name Name of the memory range, used in C header generation
  * @param description Custom description to help SW developers
  * @param bitfields Special options for specific bitfields inside the memory range
  * @param stall_sig Dedicated stall signal to indicate that this address must wait to accept/produce data
  * @param write_en_sig Optional enable signal for storing a signal to AXI register. Used for read-only registers.
  * @param decoupled_inst DecoupledIO instance to use
  */
class MemoryRange(val begin: BigInt, 
                  var end: BigInt = 0, 
                  var depth: Int = 0,
                  val mode: MemoryMode.MemoryMode = MemoryMode.RW,
                  val cached: Boolean = false,
                  var init: BigInt = 0,
                  val decoupled: Boolean = false,
                  val name: String = "",
                  val description: String = "",
                  val bitfields: List[BitField] = List[BitField](),
                  val read_stall_sig: Bool = false.B,
                  val write_stall_sig: Bool = false.B,
                  val write_en_sig: Bool = true.B,
                  val decoupled_inst: Option[DecoupledIO[UInt]] = None
) {


  // assert valid address range bounds
  require(depth != 0 || end != 0, "Either depth or end must be a non-zero value")
  require(depth == 0 || end == 0, "Define the memory range using either depth or end, not both")
  
  // Calculate end address from depth parameter
  // or calculate depth from end address
  if (depth != 0) end = begin + (1 << depth) - 1
  else if (end != 0) depth = log2Ceil(end - begin)

  if (decoupled && (mode == MemoryMode.RW)) {
    throw new IllegalArgumentException("Cannot have decoupled && MemoryMode.RW")
  }

  require(begin <= end, "Invalid range: [0x%08x, 0x%08x]".format(begin, end))

  /* Number of bits */
  val word_size = ((end - begin + 1) * 8).toInt

  /* Initialization value in Chisel UInt type. Includes BitField init values */
  def initChisel = init.U(word_size.W)

  /* init field as a Chisel UInt */
  def initWire = WireInit(init.U(word_size.W))

  /* A wire that asserts when the memory range register is written.
   * If there is a stall, this wire asserts only after the stall has 
   * been cleared.
   */
  var writeStrobe = false.B


  /* A wire that asserts when the memory range register is accessed with write.
   * If there is a stall, this wire asserts at the first access.
   */
  var writeAccess = false.B
  
  /* DEPRECATED && DYSFUNCTIONAL:
   * A wire that asserts when the memory range register is read.
   * If there is a stall, this wire asserts only after the stall has 
   * been cleared.
   */
  var readStrobe = false.B
  /* DEPRECATED && DYSFUNCTIONAL: 
   * A wire that asserts when the memory range register is accessed with read.
   * If there is a stall, this wire asserts at the first access.
   */
  var readAccess = false.B

  // Combine BitField init values with the provided init value
  if (!bitfields.isEmpty) {
    for ((bitfield, i) <- bitfields.zipWithIndex) {
      init = (init & ~bitfield.mask) | bitfield.shiftedVal
    }
  }

  override def toString : String = s"MemoryRange [0x%08x, 0x%08x] $mode".format(begin, end)
}



object MemoryRange {
  /** Define convenience methods for String type */
  implicit class StringHelpers(str: String) {
    /** Convert number from chisel string representation to BigInt */
    def toBigInt = {
      val (base, num) = str.splitAt(1)
      val radix = base match {
        case "x" | "h" => 16
        case "d" => 10
        case "o" => 8
        case "b" => 2
        case _ => throw new IllegalArgumentException(s"""Could not parse number from string "$str", invalid base '$base'""")
      }
      BigInt(num.filterNot(_ == '_'), radix)
    }
  }

  def apply[T <: Any](begin: T, 
                      end: T = 0, 
                      depth: Int = 0,
                      mode: MemoryMode.MemoryMode = MemoryMode.RW,
                      cached: Boolean = false,
                      init: BigInt = 0,
                      name: String = "",
                      decoupled: Boolean = false,
                      description: String = "",
                      bitfields: List[BitField] = List[BitField](),
                      read_stall_sig: Bool = false.B,
                      write_stall_sig: Bool = false.B,
                      write_en_sig: Bool = true.B,
                      decoupled_inst: Option[DecoupledIO[UInt]] = None) = {
    val convertBegin: BigInt = begin match {
      case a: BigInt => a
      case a: Int => a
      case a: String => a.toBigInt
      case _: Any => throw new IllegalArgumentException("Unsupported type for begin address!")
    }
    val convertEnd: BigInt = end match {
      case a: BigInt => a
      case a: Int => a
      case a: String => a.toBigInt
      case _: Any => throw new IllegalArgumentException("Unsupported type for end address!")
    }
    new MemoryRange(convertBegin, convertEnd, depth = depth, mode = mode, cached = cached, init = init,
                    name = name, bitfields = bitfields, description = description, read_stall_sig = read_stall_sig,
                    write_stall_sig = write_stall_sig, write_en_sig = write_en_sig,
                    decoupled = decoupled, decoupled_inst = decoupled_inst)
  }

}

