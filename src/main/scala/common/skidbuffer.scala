// SPDX-License-Identifier: Apache-2.0

package amba.common

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

class SkidBufferIO[T <: Data](gen: T) extends Bundle {
  val in  = Flipped(new IrrevocableIO(gen))
  val out = new IrrevocableIO(gen)
}

/** Skid buffer for Ready/Valid handshaked channels.
  * If out.ready is low, data is stored to a buffer register
  * @param gen Channel data type, e.g. an instance of some custom Bundle.
  * @param cut Cut combinational path
  */
class SkidBuffer[T <: Data](gen: T, cut: Boolean = false) extends Module {
  val io = IO(new SkidBufferIO(gen))


  if (!cut) {
    val bits_buffer = RegEnable(next = io.in.bits, enable = io.in.ready)
    val valid_buffer = RegInit(false.B)

    when ((io.in.valid && io.in.ready) && (io.out.valid && !io.out.ready)) {
      valid_buffer := true.B
    } .elsewhen (io.out.ready) {
      valid_buffer := false.B
    }
    io.in.ready := !valid_buffer
    io.out.valid := valid_buffer || io.in.valid
    io.out.bits := Mux(valid_buffer, bits_buffer, io.in.bits)
  } else {
    val bits_buffer = Module(new Queue(gen, 2))
    bits_buffer.io.enq <> io.in
    bits_buffer.io.deq <> io.out
  }
}


class SkidBufferFormalProperties[T <: Data](gen: T) extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle {
    val buffer = Flipped(new SkidBufferIO(gen))
    val clock = Input(Bool())
    val reset = Input(Bool())
  })
  addResource("/SkidBufferFormalProperties.v")
} 

class SkidBufferFormalTest[T <: Data](gen: T) extends Module {
  val sb = Module(new SkidBuffer(gen))
  val properties = Module(new SkidBufferFormalProperties(gen))

  sb.io <> properties.io.buffer
  properties.io.clock := clock.asBool
  properties.io.reset := reset.asBool
}

object SkidBuffer extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new SkidBuffer(UInt(32.W))))
  (new ChiselStage).execute(args, annos)
}

object SkidBufferFormalTest extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new SkidBufferFormalTest(UInt(32.W))))
  (new ChiselStage).execute(args, annos)
}