// OBS: sbt._ has also process. Importing scala.sys.process
// and explicitly using it ensures the correct operation
import scala.sys.process._
import java.io.{File, PrintWriter}
import com.gilcloud.sbt.gitlab.{GitlabCredentials,GitlabPlugin}

ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / organization     := "Chisel-blocks"

val chiselVersion = "3.5.1"

// GitLab plugin options
GitlabPlugin.autoImport.gitlabGroupId     :=  Some(13348068)
GitlabPlugin.autoImport.gitlabProjectId   :=  Some(33716184)
GitlabPlugin.autoImport.gitlabDomain      :=  "gitlab.com"

// sbt-git plugin options
git.useGitDescribe := true

// DO NOT ADD YOUR TOKEN TO VERSION CONTROL
// This is here only because the plugin needs some credentials, be they valid or not, or else it throws an error
credentials += {
  val tmpfile = File.createTempFile(".credentials", ".txt")
  val writer = new PrintWriter(tmpfile)
  writer.write(s"""
  |realm=gitlab
  |host=my-gitlab-host
  |user=Private-Token
  |password=password
  """.stripMargin)
  writer.close()
  Credentials(tmpfile)
  }

lazy val amba = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "amba",

    // Manually set version if needed
    // version := "0.0.2",
    // isSnapshot := true,

    libraryDependencies ++= Seq(
      "edu.berkeley.cs" %% "chisel3" % chiselVersion,
      "edu.berkeley.cs" %% "chiseltest" % "0.5.1"
    ),
    scalacOptions ++= Seq(
      "-language:reflectiveCalls",
      "-deprecation",
      "-feature",
      "-Xcheckinit",
      "-P:chiselplugin:genBundleElements",
    ),
    addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % chiselVersion cross CrossVersion.full),
  )

// Parse the version of a submodle from the git submodule status
// for those modules not version controlled by Maven or equivalent
def gitSubmoduleHashSnapshotVersion(submod: String): String = {
    val shellcommand =  "git submodule status | grep %s | awk '{print substr($1,0,7)}'".format(submod)
    scala.sys.process.Process(Seq("/bin/sh", "-c", shellcommand )).!!.mkString.replaceAll("\\s", "")+"-SNAPSHOT"
}

// Put your git-version controlled snapshots here
// libraryDependencies += "Chisel-blocks" %% "someblock" % gitSubmoduleHashSnapshotVersion("someblock")
