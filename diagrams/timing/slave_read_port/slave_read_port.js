{
  signal: [
    {name: 'ACLK', wave: 'p....|....'},
    {},
    {name: 'ARVALID', wave: '01...|..0.'},
    {name: 'ARREADY', wave: '1...0|.1..'},
    {name: 'ARADDR', wave:  'x3456|..x.'},
    {name: 'buf', wave:  'x...5..x..'},
    
    {},
    {name: 'RVALID', wave: '0.1..|...0'},
    {name: 'RREADY', wave: '1..0.|1...'},
    {name: 'RDATA', wave:  'x.34.|.56x'},
    {name: 'ren', wave: '01.0..1.0.'},
    {name: 'read_insert', wave: '01..0..10.'},
    {name: 'state', wave: '2.2.2..2.2', data: ['empty', 'busy', 'full', 'busy', 'empty']}
  ],
  config: { hscale: 2 }
}
